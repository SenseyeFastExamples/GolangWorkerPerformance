package scylladb

import (
	"github.com/juju/errors"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
	"workerperformance/components/consts"
	"workerperformance/models/skylladb"
)

var (
	once = new(sync.Once)
)

func TestMatch(t *testing.T) {
	client, err := ClientByTimeout(5 * time.Second)
	if !assert.NoError(t, err) {
		return
	}
	defer client.Close()

	err = ResetWithIndex(client)
	if !assert.NoError(t, err) {
		return
	}

	const hour = 3600
	now := now()
	before1h := now - hour
	after1h := now + hour

	w1t1e1 := scylladb.Subscriber{
		ID:          1,
		NotifyAfter: now,
		//Partner:     11110001,
		//Stream:      11110002,
		//Widget:      11110003,
		WorkerID: 1,
		Timezone: 1,
		//OS:          1,
		//Browser:     1,
		//Country:     "UA",
		//Language:    "uk",
		Endpoint: "w1-t1-e1",
	}
	w1t1e2 := scylladb.Subscriber{
		ID:          2,
		NotifyAfter: now,
		//Partner:     22220001,
		//Stream:      22220002,
		//Widget:      22220003,
		WorkerID: 1,
		Timezone: 1,
		//OS:          2,
		//Browser:     2,
		//Country:     "UA",
		//Language:    "uk",
		Endpoint: "w1-t1-e2",
	}
	w1t1e3 := scylladb.Subscriber{
		ID:          3,
		NotifyAfter: before1h,
		//Partner:     33330001,
		//Stream:      33330002,
		//Widget:      33330003,
		WorkerID: 1,
		Timezone: 1,
		//OS:          2,
		//Browser:     2,
		//Country:     "UA",
		//Language:    "uk",
		Endpoint: "w1-t1-e3",
	}
	subscribers := []scylladb.Subscriber{
		// matched
		w1t1e1,
		// matched
		w1t1e2,
		// matched
		w1t1e3,
		// skipped
		{
			ID:          4,
			WorkerID:    1,
			Timezone:    1,
			NotifyAfter: after1h,
			Endpoint:    "w1-t1-e4",
		},
		// skipped
		{
			ID:          5,
			WorkerID:    1,
			Timezone:    2,
			NotifyAfter: now,
			Endpoint:    "w1-t2-e5",
		},
		// skipped
		{
			ID:          6,
			WorkerID:    2,
			Timezone:    1,
			NotifyAfter: now,
			Endpoint:    "w2-t1-e6",
		},
	}

	err = Fill(client, subscribers)
	if !assert.NoError(t, err) {
		return
	}

	expect := []scylladb.Subscriber{w1t1e1, w1t1e2, w1t1e3}

	// first match
	actual, err := Match(client, 1, []uint8{1}, now, 10)
	if !assert.NoError(t, err) {
		return
	}

	assertSameSubscribers(t, expect, actual)

	err = Update(client, []uint32{w1t1e1.ID, w1t1e3.ID}, 1, 1, after1h)
	if !assert.NoError(t, err) {
		return
	}

	expect = []scylladb.Subscriber{w1t1e2}

	// match after update
	actual, err = Match(client, 1, []uint8{1}, now, 10)
	if !assert.NoError(t, err) {
		return
	}

	assertSameSubscribers(t, expect, actual)
}

func BenchmarkFillBatch(b *testing.B) {
	benchmarkFill(b, FillBatch)
}

func BenchmarkFillBatchByPrepare(b *testing.B) {
	benchmarkFill(b, FillBatchByPrepare)
}

func benchmarkFill(b *testing.B, fill FillHandle) {
	b.Helper()

	client, err := Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	err = ResetWithIndex(client)
	if !assert.NoError(b, err) {
		return
	}

	from := 0
	pack := 1000
	buffer := make([]scylladb.Subscriber, 0, pack)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		subscribers := GenerateAppend(buffer[:0], from, consts.WorkerCount, consts.TimezoneCount, pack)

		err := fill(client, subscribers)
		if !assert.NoError(b, err) {
			return
		}

		from += pack
	}
	b.StopTimer()

	count, err := Count(client)
	if !assert.NoError(b, err) {
		return
	}

	assert.Equal(b, b.N*pack, count)
}

/**
1e5:
BenchmarkMatch   	    5000	   7011458 ns/op	 1317351 B/op	    4530 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:194: match count 1 of b.N = 1, match document count 1260 of 8000
    main_test.go:194: match count 100 of b.N = 100, match document count 111682 of 800000
    main_test.go:194: match count 5000 of b.N = 5000, match document count 5585160 of 40000000
PASS
ok  	workerperformance/components/scylladb	35.789s
1e6:
BenchmarkMatch   	    1000	  38801484 ns/op	 5888443 B/op	   32820 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:202: match count 1 of b.N = 1, match document count 8000 of 8000
    main_test.go:202: match count 100 of b.N = 100, match document count 800000 of 800000
    main_test.go:202: match count 500 of b.N = 500, match document count 4000000 of 4000000
    main_test.go:202: match count 1000 of b.N = 1000, match document count 8000000 of 8000000
*/
func BenchmarkMatch(b *testing.B) {
	client, err := Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	if true {
		once.Do(func() {
			if true {
				err = ResetWithIndex(client)
				if err != nil {
					err = errors.Trace(err)

					return
				}
			}

			const shift = 0
			const want = 1e3

			subscribers := Generate(shift, consts.WorkerCount, consts.TimezoneCount, want-shift)
			err = FillBatch(client, subscribers)
			if err != nil {
				err = errors.Trace(err)

				return
			}
		})
	}

	if !assert.NoError(b, err) {
		return
	}

	now := now()

	matchCount := 0
	matchDocumentCount := 0

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		result, err := Match(client, uint8(i%consts.WorkerCount), timezones(i), now, consts.Limit)

		assert.NoError(b, err)
		documentCount := len(result)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N)
}

func assertSameSubscribers(t *testing.T, expect, actual []scylladb.Subscriber) {
	t.Helper()

	if !assert.Equal(t, len(expect), len(actual)) {
		return
	}

	assert.Equal(t, idMap(expect), idMap(actual))
}

func idMap(subscriber []scylladb.Subscriber) map[uint32]scylladb.Subscriber {
	result := make(map[uint32]scylladb.Subscriber)

	for _, subscriber := range subscriber {
		result[subscriber.ID] = subscriber
	}

	return result
}

func now() uint32 {
	return uint32(time.Now().Truncate(time.Second).Unix())
}

func timezones(i int) []uint8 {
	result := make([]uint8, 8)

	for j := 0; j < 8; j++ {
		current := i + j

		result[j] = uint8(current * current % consts.TimezoneCount)
	}

	return result
}
