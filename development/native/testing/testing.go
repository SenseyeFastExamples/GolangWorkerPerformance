package main

import (
	"github.com/juju/errors"
	"time"
	"workerperformance/components/logger"
	"workerperformance/components/mysql"
	"workerperformance/components/native"
)

func main() {
	connection, err := mysql.Connection()
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
	defer connection.Close()

	start := time.Now()

	documents := native.Run(connection)

	logger.Infof("success %d complete by time %d nanoseconds", documents, time.Now().Sub(start))
}
