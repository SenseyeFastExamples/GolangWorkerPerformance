package mysql

import (
	"database/sql"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/etalon"
	"workerperformance/models/mysql"
)

var (
	once = new(sync.Once)
)

func TestMatch(t *testing.T) {
	connection, err := Connection()
	if !assert.NoError(t, err) {
		return
	}
	defer connection.Close()

	err = Reset(connection)
	if !assert.NoError(t, err) {
		return
	}

	count, err := tableCount(connection)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, 0, count)

	const hour = 3600
	now := now()
	before1h := now - hour
	after1h := now + hour

	w1t1e1 := mysql.Subscriber{
		ID:          1,
		NotifyAfter: now,
		Partner:     11110001,
		Stream:      11110002,
		Widget:      11110003,
		WorkerID:    1,
		Timezone:    1,
		OS:          1,
		Browser:     1,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    "w1-t1-e1",
	}
	w1t1e2 := mysql.Subscriber{
		ID:          2,
		NotifyAfter: now,
		Partner:     22220001,
		Stream:      22220002,
		Widget:      22220003,
		WorkerID:    1,
		Timezone:    1,
		OS:          2,
		Browser:     2,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    "w1-t1-e2",
	}
	w1t1e3 := mysql.Subscriber{
		ID:          3,
		NotifyAfter: before1h,
		Partner:     33330001,
		Stream:      33330002,
		Widget:      33330003,
		WorkerID:    1,
		Timezone:    1,
		OS:          2,
		Browser:     2,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    "w1-t1-e3",
	}
	subscribers := []mysql.Subscriber{
		// matched
		w1t1e1,
		// matched
		w1t1e2,
		// matched
		w1t1e3,
		// skipped
		{
			ID:          4,
			WorkerID:    1,
			Timezone:    1,
			NotifyAfter: after1h,
			Endpoint:    "w1-t1-e4",
		},
		// skipped
		{
			ID:          5,
			WorkerID:    1,
			Timezone:    2,
			NotifyAfter: now,
			Endpoint:    "w1-t2-e5",
		},
		// skipped
		{
			ID:          6,
			WorkerID:    2,
			Timezone:    1,
			NotifyAfter: now,
			Endpoint:    "w2-t1-e6",
		},
	}

	err = Fill(connection, subscribers)
	if !assert.NoError(t, err) {
		return
	}

	count, err = tableCount(connection)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, len(subscribers), count)

	expect := []mysql.Subscriber{w1t1e1, w1t1e2, w1t1e3}

	// assert index right
	err = IndexFull(connection)
	if !assert.NoError(t, err) {
		return
	}

	// first match
	actual, err := Match(connection, 1, []int{1}, now, 10)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)

	err = Update(connection, []uint32{w1t1e1.ID, w1t1e3.ID}, after1h)
	if !assert.NoError(t, err) {
		return
	}

	expect = []mysql.Subscriber{w1t1e2}

	// match after update
	actual, err = Match(connection, 1, []int{1}, now, 10)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)
}

/**
1e5:
BenchmarkMatch   	   10000	   2098259 ns/op	  963557 B/op	   10526 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:212: match count 1 of b.N = 1, match document count 638 of 8000
    main_test.go:212: match count 100 of b.N = 100, match document count 55795 of 800000
    main_test.go:212: match count 10000 of b.N = 10000, match document count 5583790 of 80000000
PASS
ok  	workerperformance/components/mysql	31.553s
1e6:
BenchmarkMatch   	     500	  74543949 ns/op	 4377291 B/op	  104906 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:219: match count 1 of b.N = 1, match document count 6121 of 8000
    main_test.go:219: match count 100 of b.N = 100, match document count 557099 of 800000
    main_test.go:219: match count 500 of b.N = 500, match document count 2787231 of 4000000
PASS
ok  	workerperformance/components/mysql	141.309s
*/
func BenchmarkMatch(b *testing.B) {
	connection, err := Connection()
	if !assert.NoError(b, err) {
		return
	}
	defer connection.Close()

	if true {
		once.Do(func() {
			err = Reset(connection)
			if err != nil {
				return
			}

			subscribers := etalon.Generate(0, consts.WorkerCount, consts.TimezoneCount, 1e3)
			err = FillBatch(connection, subscribers)

			if err != nil {
				return
			}

			err = IndexFull(connection)
		})

		if !assert.NoError(b, err) {
			return
		}
	}

	benchmarkMatch(b, connection)
}

func benchmarkMatch(b *testing.B, connection *sql.DB) {
	now := now()

	matchCount := 0
	matchDocumentCount := 0

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		result, err := Match(connection, uint8(i%consts.WorkerCount), timezones(i), now, consts.Limit)

		assert.NoError(b, err)
		documentCount := len(result)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N)
}

func timezones(i int) []int {
	result := make([]int, 8)

	for j := 0; j < 8; j++ {
		current := i + j

		result[j] = current * current % consts.TimezoneCount
	}

	return result
}

func now() uint32 {
	return uint32(time.Now().Truncate(time.Second).Unix())
}
