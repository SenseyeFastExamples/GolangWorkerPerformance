package aerospike

import (
	"github.com/aerospike/aerospike-client-go"
)

const (
	Namespace                    = "rtb"
	SetName                      = "subscribers"
	workerTimezoneNotifyMergeBin = "wr_tz_ny" // Bin name length greater than 14 characters, or maximum number of unique bin names are exceeded
	idBin                        = "id"
	countryBin                   = "country"
	languageBin                  = "language"
	endpointBin                  = "endpoint"
)

func key(subscriberID uint32) *aerospike.Key {
	result, _ := aerospike.NewKey(Namespace, SetName, aerospike.NewIntegerValue(int(subscriberID)))

	return result
}
