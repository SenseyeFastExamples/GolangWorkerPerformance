package mongodb

import (
	"context"
	"github.com/juju/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"workerperformance/components/logger"
	"workerperformance/models/mongodb"
)

func collectionCount(collection *mongo.Collection) (int64, error) {
	return collection.CountDocuments(context.TODO(), bson.M{})
}

func Match(dst []mongodb.Subscriber, collection *mongo.Collection, workerID int, timezones []int, now int32, limit int) ([]mongodb.Subscriber, error) {
	cursor, err := find(collection, workerID, timezones, now, limit)

	if err != nil {
		return nil, err
	}

	result := dst
	for cursor.Next(context.TODO()) {
		var subscriber mongodb.Subscriber
		err := cursor.Decode(&subscriber)

		if err != nil {
			return nil, err
		}

		result = append(result, subscriber)
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	if err = cursor.Close(context.TODO()); err != nil {
		logger.Error(errors.Trace(err))

		// NOP
	}

	return result, nil
}

func Update(collection *mongo.Collection, ids []int32, notifyAfter int32) error {
	_, err := collection.UpdateMany(
		context.TODO(),
		bson.M{
			"_id": bson.M{
				"$in": ids,
			},
		},
		bson.M{
			"$set": bson.M{
				"notify_after": notifyAfter,
			},
		},
	)

	return err
}

func Max(collection *mongo.Collection) (uint32, error) {
	findOptions := options.Find().SetProjection(bson.D{{"_id", 1}}).SetLimit(1).SetSort(bson.M{
		"_id": -1,
	})

	cursor, err := collection.Find(context.TODO(), bson.M{}, findOptions)
	if err != nil {
		return 0, err
	}

	max := uint32(0)

	for cursor.Next(context.TODO()) {
		var subscriber mongodb.SubscriberID
		err := cursor.Decode(&subscriber)

		if err != nil {
			return 0, err
		}

		max = subscriber.ID
	}

	if err := cursor.Err(); err != nil {
		return 0, err
	}

	if err = cursor.Close(context.TODO()); err != nil {
		logger.Error(errors.Trace(err))

		// NOP
	}

	return max, nil
}

func matchQueryTime(collection *mongo.Collection, workerID int, timezones []int, now int32, limit int) (int, error) {
	cursor, err := find(collection, workerID, timezones, now, limit)

	if err != nil {
		return 0, err
	}

	count := 0
	for cursor.Next(context.TODO()) {
		count++
	}

	if err := cursor.Err(); err != nil {
		return 0, err
	}

	if err = cursor.Close(context.TODO()); err != nil {
		logger.Error(errors.Trace(err))

		// NOP
	}

	return count, nil
}

func find(collection *mongo.Collection, workerID int, timezones []int, now int32, limit int) (*mongo.Cursor, error) {
	findOptions := options.Find().SetLimit(int64(limit))

	return collection.Find(
		context.TODO(),
		bson.M{
			"worker_id": workerID,
			"timezone": bson.M{
				"$in": timezones,
			},
			"notify_after": bson.M{
				"$lte": now,
			},
		},
		findOptions,
	)
}
