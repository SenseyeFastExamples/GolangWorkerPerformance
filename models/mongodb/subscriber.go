package mongodb

type Subscriber struct {
	ID          int32  `bson:"_id"`          // as int32 for Number, uint32 in MongoDB as NumberLong
	NotifyAfter int32  `bson:"notify_after"` // as int32 for Number, uint32 in MongoDB as NumberLong
	Partner     int32  `bson:"partner"`      // as int32 for Number, uint32 in MongoDB as NumberLong
	Stream      int32  `bson:"stream"`       // as int32 for Number, uint32 in MongoDB as NumberLong
	Widget      int32  `bson:"widget"`       // as int32 for Number, uint32 in MongoDB as NumberLong
	WorkerID    uint8  `bson:"worker_id"`
	Timezone    uint8  `bson:"timezone"`
	OS          uint8  `bson:"os"`
	Browser     uint8  `bson:"browser"`
	Country     string `bson:"country"`
	Language    string `bson:"language"`
	Endpoint    string `bson:"endpoint"`
}

type SubscriberID struct {
	ID uint32 `bson:"_id"`
}

func (s *Subscriber) UnmarshalBSON(data []byte) error {
	return Unmarshal(data, s)
}
