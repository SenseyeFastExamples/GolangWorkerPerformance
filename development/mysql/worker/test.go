package main

import (
	"database/sql"
	"github.com/juju/errors"
	"io/ioutil"
	"sync"
	"time"
	"workerperformance/components/logger"
	"workerperformance/components/mysql"
	"workerperformance/components/oplogger"
)

const (
	WorkerCount   = 20
	TimezoneCount = 24
	Batch         = 8000
	NotifyTime    = 5 * time.Second
	RestTime      = 5 * time.Second
	UpdateShift   = 20 // seconds
	N             = 50
)

var (
	operationLogger = oplogger.NewLogger(WorkerCount * 2 * N)
)

func main() {
	connections := make([]*sql.DB, WorkerCount)

	for i := 0; i < WorkerCount; i++ {
		connection, err := mysql.Connection()
		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
		defer connection.Close()
		connections[i] = connection
	}

	start := time.Now()
	logger.Infof("start %d workers with %d iterations", WorkerCount, N)

	wg := new(sync.WaitGroup)
	wg.Add(WorkerCount)
	for i := 0; i < WorkerCount; i++ {
		go func(index int) {
			worker(connections[index], index)

			wg.Done()
		}(i)
	}

	wg.Wait()

	duration := time.Since(start)
	logger.Infof("worker complete by %d nanoseconds", duration)

	err := ioutil.WriteFile("/go/src/workerperformance/result/worker-mysql.txt", operationLogger.Dump(), 0666)
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}

	err = operationLogger.DumpPlot(
		WorkerCount,
		N,
		"/go/src/workerperformance/result/worker-mysql-partition-match.png",
		"/go/src/workerperformance/result/worker-mysql-partition-update.png",
		"/go/src/workerperformance/result/worker-mysql-partition-complex.png",
	)
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
}

func worker(connection *sql.DB, workerID int) {
	for i := 0; i < N; i++ {
		logger.Infof("worker %d start %d iteration", workerID, i)

		start := time.Now()
		subscribers, err := mysql.Match(connection, workerID, timezones(i), now(), Batch)
		duration := time.Now().Sub(start)

		logger.Error(errors.Trace(err))

		success := err == nil

		length := len(subscribers)

		operationLogger.LogMatch(
			int32(start.Unix()),
			workerID,
			i,
			length,
			int64(duration),
			success,
		)

		time.Sleep(NotifyTime)

		if success {
			ids := make([]uint32, length)
			for j := 0; j < length; j++ {
				ids[j] = subscribers[j].ID
			}

			start := time.Now()
			err = mysql.Update(connection, ids, now()+UpdateShift)
			duration := time.Now().Sub(start)

			success := err == nil
			operationLogger.LogUpdate(
				int32(start.Unix()),
				workerID,
				i,
				length,
				int64(duration),
				success,
			)
		}

		time.Sleep(RestTime)
	}
}

func timezones(i int) []int {
	result := make([]int, 8)

	for j := 0; j < 8; j++ {
		current := i + j

		result[j] = current % TimezoneCount
	}

	return result
}

func now() uint32 {
	return uint32(time.Now().Truncate(time.Second).Unix())
}
