package scylladb

import (
	"errors"
	"github.com/gocql/gocql"
	"time"
	"workerperformance/components/env"
)

var (
	ErrEmptyHost = errors.New("ScyllaDB empty hosts")
)

/**
CREATE KEYSPACE rtb
WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 1};
*/

func Client() (*gocql.Session, error) {
	return ClientByTimeout(time.Second)
}

func ClientByTimeout(timeout time.Duration) (*gocql.Session, error) {
	hosts := env.ScyllaDBURIs()
	if len(hosts) == 0 {
		return nil, ErrEmptyHost
	}

	cluster := gocql.NewCluster(hosts...)
	cluster.Keyspace = "rtb"
	cluster.Consistency = gocql.One
	cluster.Timeout = timeout

	return cluster.CreateSession()
}
