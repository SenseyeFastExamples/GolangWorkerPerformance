package mongodb

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"workerperformance/components/env"
)

func Client() (*mongo.Client, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(env.MongoDBURI()))

	if err != nil {
		return nil, err
	}

	err = client.Connect(context.TODO())

	if err != nil {
		return nil, err
	}

	return client, nil
}
