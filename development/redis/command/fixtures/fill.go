package main

import (
	"workerperformance/components/common-redis-command"
	"workerperformance/components/redis"
)

func main() {
	command.FillCommand(redis.ClientByNode, redis.FlushDB, redis.StoreShardHashes)
}
