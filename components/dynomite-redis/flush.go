package redis

import (
	"github.com/go-redis/redis"
	"github.com/juju/errors"
)

func FlushDB(client *redis.Client) error {
	keys, err := client.Keys("*").Result()

	if err != nil {
		return errors.Trace(err)
	}

	if len(keys) == 0 {
		return nil
	}

	return client.Del(keys...).Err()
}
