package mongodb

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
	"sync"
	"testing"
	"time"
	"workerperformance/components/consts"
	"workerperformance/models/mongodb"
)

const (
	count = 1e6
)

var (
	once = new(sync.Once)
)

func TestMatch(t *testing.T) {
	client, err := Client()
	if !assert.NoError(t, err) {
		return
	}
	defer client.Disconnect(context.TODO())

	collection := Collection(client)

	err = Drop(collection)
	if !assert.NoError(t, err) {
		return
	}

	max, err := Max(collection)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, uint32(0), max)

	const hour = 3600
	now := now()
	before1h := now - hour
	after1h := now + hour

	w1t1e1 := mongodb.Subscriber{
		ID:          1,
		NotifyAfter: now,
		Partner:     11110001,
		Stream:      11110002,
		Widget:      11110003,
		WorkerID:    1,
		Timezone:    1,
		OS:          1,
		Browser:     1,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    "w1-t1-e1",
	}
	w1t1e2 := mongodb.Subscriber{
		ID:          2,
		NotifyAfter: now,
		Partner:     22220001,
		Stream:      22220002,
		Widget:      22220003,
		WorkerID:    1,
		Timezone:    1,
		OS:          2,
		Browser:     2,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    "w1-t1-e2",
	}
	w1t1e3 := mongodb.Subscriber{
		ID:          3,
		NotifyAfter: before1h,
		Partner:     33330001,
		Stream:      33330002,
		Widget:      33330003,
		WorkerID:    1,
		Timezone:    1,
		OS:          2,
		Browser:     2,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    "w1-t1-e3",
	}
	subscribers := []mongodb.Subscriber{
		// matched
		w1t1e1,
		// matched
		w1t1e2,
		// matched
		w1t1e3,
		// skipped
		{
			ID:          4,
			WorkerID:    1,
			Timezone:    1,
			NotifyAfter: after1h,
			Endpoint:    "w1-t1-e4",
		},
		// skipped
		{
			ID:          5,
			WorkerID:    1,
			Timezone:    2,
			NotifyAfter: now,
			Endpoint:    "w1-t2-e5",
		},
		// skipped
		{
			ID:          6,
			WorkerID:    2,
			Timezone:    1,
			NotifyAfter: now,
			Endpoint:    "w2-t1-e6",
		},
	}

	err = Fill(collection, subscribers)
	if !assert.NoError(t, err) {
		return
	}

	count, err := collectionCount(collection)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, int64(len(subscribers)), count)

	err = Index(collection)
	if !assert.NoError(t, err) {
		return
	}

	expect := []mongodb.Subscriber{w1t1e1, w1t1e2, w1t1e3}

	buffer := make([]mongodb.Subscriber, 0, 10)

	// first match
	actual, err := Match(buffer[:0], collection, 1, []int{1}, now, 10)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)

	err = Update(collection, []int32{w1t1e1.ID, w1t1e3.ID}, after1h)
	if !assert.NoError(t, err) {
		return
	}

	expect = []mongodb.Subscriber{w1t1e2}

	// match after update
	actual, err = Match(buffer[:0], collection, 1, []int{1}, now, 10)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)

	max, err = Max(collection)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, uint32(6), max)
}

/**
1e5:
BenchmarkMatch   	    3000	  10630428 ns/op	 5187138 B/op	   42678 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:265: match count 1 of b.N = 1, match document count 577 of 8000
    main_test.go:265: match count 100 of b.N = 100, match document count 54937 of 800000
    main_test.go:265: match count 3000 of b.N = 3000, match document count 1650350 of 24000000
PASS
ok  	workerperformance/components/mongodb	35.050s
1e6:
BenchmarkMatch   	     500	  76433153 ns/op	49004393 B/op	  428886 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:265: match count 1 of b.N = 1, match document count 6352 of 8000
    main_test.go:265: match count 100 of b.N = 100, match document count 556216 of 800000
    main_test.go:265: match count 500 of b.N = 500, match document count 2782692 of 4000000
PASS
ok  	workerperformance/components/mongodb	63.837s
*/
func BenchmarkMatch(b *testing.B) {
	client, err := Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Disconnect(context.TODO())

	collection := Collection(client)

	// to fill many documents once by benchmark 1, 10, ... 1 000 000
	err = fillSubscribers(collection)

	if !assert.NoError(b, err) {
		return
	}

	benchmarkMatch(b, collection)
}

func BenchmarkMatchQueryTime(b *testing.B) {
	client, err := Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Disconnect(context.TODO())

	collection := Collection(client)

	// to fill many documents once by benchmark 1, 10, ... 1 000 000
	err = fillSubscribers(collection)

	if !assert.NoError(b, err) {
		return
	}

	benchmarkMatchQueryTime(b, collection)
}

func fillSubscribers(collection *mongo.Collection) error {
	// on fill by fixtures
	if true {
		return nil
	}

	var err error

	once.Do(func() {
		subscribers := Generate(0, consts.WorkerCount, consts.TimezoneCount, count)

		err = Drop(collection)
		if err != nil {
			return
		}

		err = FillBatch(collection, subscribers)

		if err != nil {
			return
		}

		err = Index(collection)
	})

	return err
}

func benchmarkMatch(b *testing.B, collection *mongo.Collection) {
	now := now()

	matchCount := 0
	matchDocumentCount := 0

	buffer := make([]mongodb.Subscriber, 0, consts.Limit)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		result, err := Match(buffer[:0], collection, i%consts.WorkerCount, timezones(i), now, consts.Limit)

		assert.NoError(b, err)
		documentCount := len(result)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N)
}

func benchmarkMatchQueryTime(b *testing.B, collection *mongo.Collection) {
	now := now()

	matchCount := 0
	matchDocumentCount := 0

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		documentCount, err := matchQueryTime(collection, i%consts.WorkerCount, timezones(i), now, consts.Limit)

		assert.NoError(b, err)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N)
}

func timezones(i int) []int {
	result := make([]int, 8)

	for j := 0; j < 8; j++ {
		current := i + j

		result[j] = current * current % consts.TimezoneCount
	}

	return result
}

func now() int32 {
	return int32(time.Now().Truncate(time.Second).Unix())
}
