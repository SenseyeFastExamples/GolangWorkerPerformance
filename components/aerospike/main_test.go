package aerospike

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"workerperformance/components/consts"
	"workerperformance/models/aerospike"
)

func TestClient(t *testing.T) {
	client, err := Client()
	if !assert.NoError(t, err) {
		return
	}
	defer client.Close()

	subscriber := aerospike.Subscriber{
		ID:          1,
		WorkerID:    1,
		Timezone:    1,
		NotifyAfter: 1,
		Endpoint:    consts.Endpoint,
	}

	err = Store(client, subscriber)
	if !assert.NoError(t, err) {
		return
	}

	actual, err := fetch(client, subscriber.ID)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, subscriber, actual)
}

func TestMatch(t *testing.T) {
	client, err := Client()
	if !assert.NoError(t, err) {
		return
	}
	defer client.Close()

	err = client.Truncate(nil, Namespace, SetName, nil)
	if !assert.NoError(t, err) {
		return
	}

	subscribers := []aerospike.Subscriber{
		{
			ID:          1,
			WorkerID:    1,
			Timezone:    1,
			NotifyAfter: 1565178000,
			Country:     "UA",
			Language:    "uk",
			Endpoint:    consts.Endpoint,
		},
		{
			ID:          2,
			WorkerID:    1,
			Timezone:    1,
			NotifyAfter: 1565178001,
			Country:     "LT",
			Language:    "lt",
			Endpoint:    consts.Endpoint,
		},
	}

	err = StoreBatch(client, subscribers)
	if !assert.NoError(t, err) {
		return
	}

	actual, err := Match(client, 1, 1, 1565178018, 2)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, subscribers, actual)
}

func TestFill(t *testing.T) {
	t.Skip("need for init to BenchmarkMatch")

	client, err := Client()
	if !assert.NoError(t, err) {
		return
	}
	defer client.Close()

	if false {
		err = client.Truncate(nil, Namespace, SetName, nil)
		if !assert.NoError(t, err) {
			return
		}
	}

	subscribers := Generate(9e6, consts.WorkerCount, consts.TimezoneCount, 0, 1e5)
	err = StoreBatch(client, subscribers)
	if !assert.NoError(t, err) {
		return
	}

	if false {
		err = Index(client)
		if !assert.NoError(t, err) {
			return
		}
	}
}

func BenchmarkMatch(b *testing.B) {
	client, err := Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	matchCount := 0
	matchDocumentCount := 0

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		result, err := Match(client, uint8(i%consts.WorkerCount), uint8(i*i%consts.TimezoneCount), 1565178018, consts.Limit)

		assert.NoError(b, err)
		documentCount := len(result)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N)
}

/**
BenchmarkStore   	   30000	     53743 ns/op	     676 B/op	      11 allocs/op
*/
func BenchmarkStore(b *testing.B) {
	client, err := Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	err = client.Truncate(nil, Namespace, SetName, nil)
	if !assert.NoError(b, err) {
		return
	}

	subscribers := Generate(0, consts.WorkerCount, consts.TimezoneCount, 0, b.N)

	for i := 0; i < b.N; i++ {
		err = Store(client, subscribers[i])

		if !assert.NoError(b, err) {
			return
		}
	}

	// aql> SELECT wr_tz_ny FROM rtb.subscribers WHERE wr_tz_ny BETWEEN 16496969383936 AND 16498534577976;
	_ = Index(client)
}
