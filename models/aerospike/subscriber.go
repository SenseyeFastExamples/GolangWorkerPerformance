package aerospike

// http://golang-sizeof.tips/?t=Ly8gU2FtcGxlIGNvZGUKc3RydWN0IHsKCUlEICAgICAgICAgIHVpbnQzMgoJTm90aWZ5QWZ0ZXIgdWludDMyCglQYXJ0bmVyICAgICB1aW50MzIKCVN0cmVhbSAgICAgIHVpbnQzMgoJV2lkZ2V0ICAgICAgdWludDMyCglXb3JrZXJJRCAgICB1aW50OAoJVGltZXpvbmUgICAgdWludDgKCU9TICAgICAgICAgIHVpbnQ4CglCcm93c2VyICAgICB1aW50OAoJQ291bnRyeSAgICAgc3RyaW5nCglMYW5ndWFnZSAgICBzdHJpbmcKCUVuZHBvaW50ICAgIHN0cmluZwp9Cg==
type Subscriber struct {
	ID          uint32
	NotifyAfter uint32
	Partner     uint32
	Stream      uint32
	Widget      uint32
	WorkerID    uint8
	Timezone    uint8
	OS          uint8
	Browser     uint8
	Country     string
	Language    string
	Endpoint    string
}

func (s *Subscriber) Marshal() int64 {
	return WorkerTimezoneNotifyMarshal(s.WorkerID, s.Timezone, s.NotifyAfter)
}

func (s *Subscriber) Unmarshal(source int64) {
	s.WorkerID, s.Timezone, s.NotifyAfter = workerTimezoneNotifyUnmarshal(source)
}

func WorkerTimezoneNotifyMarshal(workerID, timezone uint8, notifyAfter uint32) int64 {
	return int64(workerID)<<40 | int64(timezone)<<32 | int64(notifyAfter)
}

func workerTimezoneNotifyUnmarshal(source int64) (uint8, uint8, uint32) {
	return uint8(source >> 40), uint8(source >> 32), uint32(source)
}
