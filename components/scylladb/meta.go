package scylladb

import (
	"github.com/gocql/gocql"
	"github.com/juju/errors"
)

const (
	subscribersShortTableCreate = `
CREATE TABLE subscribers
(
  id           INT,
  notify_after INT,
  worker_id    TINYINT,
  timezone     TINYINT,
  endpoint     TEXT,
  PRIMARY KEY (id, worker_id, timezone)
);
`
	subscribersFullTableCreate = `
CREATE TABLE subscribers
(
  id           INT,
  notify_after INT,
  partner      INT,
  stream       INT,
  widget       INT,
  worker_id    TINYINT,
  timezone     TINYINT,
  os           TINYINT,
  browser      TINYINT,
  country      TEXT,
  language     TEXT,
  endpoint     TEXT,
  PRIMARY KEY (id, worker_id, timezone)
);
`
	subscribersTableCreate = subscribersShortTableCreate

	subscribersTableDrop = `DROP TABLE IF EXISTS subscribers;`

	subscribersShortMaterializedViewCreate = `
CREATE MATERIALIZED VIEW subscribers_by_worker_timezone_notify AS
SELECT id,
       notify_after,
       worker_id,
       timezone,
       endpoint
FROM subscribers
WHERE worker_id IS NOT NULL
  AND timezone IS NOT NULL
  AND notify_after IS NOT NULL
  AND id IS NOT NULL
  PRIMARY KEY (worker_id, timezone, notify_after, id)
  WITH CLUSTERING ORDER BY (notify_after ASC, timezone ASC, notify_after DESC);
`

	subscribersFullMaterializedViewCreate = `
CREATE MATERIALIZED VIEW subscribers_by_worker_timezone_notify AS
SELECT id,
       notify_after,
       partner,
       stream,
       widget,
       worker_id,
       timezone,
       os,
       browser,
       country,
       language,
       endpoint
FROM subscribers
WHERE worker_id IS NOT NULL
  AND timezone IS NOT NULL
  AND notify_after IS NOT NULL
  AND id IS NOT NULL
  PRIMARY KEY (worker_id, timezone, notify_after, id)
  WITH CLUSTERING ORDER BY (notify_after ASC, timezone ASC, notify_after DESC);
`

	subscribersMaterializedViewCreate = subscribersShortMaterializedViewCreate

	subscribersMaterializedViewDrop = `DROP MATERIALIZED VIEW IF EXISTS subscribers_by_worker_timezone_notify;`

	subscribersTruncate = `TRUNCATE subscribers;`
)

func ResetWithIndex(client *gocql.Session) error {
	// already created all
	if true {
		return Truncate(client)
	}

	err := Reset(client)
	if err != nil {
		return errors.Trace(err)
	}

	err = Index(client)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

func Reset(client *gocql.Session) error {
	err := client.Query(subscribersMaterializedViewDrop).Exec()
	if err != nil {
		return errors.Trace(err)
	}

	err = client.Query(subscribersTableDrop).Exec()
	if err != nil {
		return errors.Trace(err)
	}

	err = client.Query(subscribersTableCreate).Exec()
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

func Index(client *gocql.Session) error {
	err := client.Query(subscribersMaterializedViewCreate).Exec()

	return errors.Trace(err)
}

func Truncate(client *gocql.Session) error {
	return client.Query(subscribersTruncate).Exec()
}
