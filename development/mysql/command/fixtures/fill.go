package main

import (
	"flag"
	"fmt"
	"github.com/juju/errors"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/etalon"
	"workerperformance/components/logger"
	"workerperformance/components/mysql"
)

func main() {
	helpFlag := flag.Bool("help", false, "help")
	resetFlag := flag.Bool("reset", false, "MySQL drop & create table")
	indexFlag := flag.Bool("index", false, "MySQL add index")
	shiftFlag := flag.Int("shift", 0, "shift")
	limitFlag := flag.Int("n", 0, "generate & fill MySQL table")

	flag.Parse()

	help := *helpFlag
	reset := *resetFlag
	index := *indexFlag
	shift := *shiftFlag
	limit := *limitFlag

	if help || (reset == false && limit == 0 && index == false) {
		flag.PrintDefaults()
		fmt.Println("example run command: fill -reset -n=1000000 -index")
		fmt.Println("example run command: fill -reset -n=1000000 -shift=1000000 -index")
		fmt.Println("example run command: fill -n=1000000")
		fmt.Println("example run command: fill -index")

		return
	}

	start := time.Now()

	connection, err := mysql.Connection()
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
	defer connection.Close()

	if reset {
		start := time.Now()
		err := mysql.Reset(connection)
		duration := time.Since(start)
		logger.Infof("MySQL drop & create table by %d nanoseconds", int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	if limit > 0 {
		start := time.Now()
		subscribers := etalon.Generate(shift, consts.WorkerCount, consts.TimezoneCount, limit)
		duration := time.Since(start)
		logger.Infof("Generate %d subscribers by %d nanoseconds", limit, int64(duration))

		start = time.Now()
		err = mysql.FillBatch(connection, subscribers)
		duration = time.Since(start)
		logger.Infof("MySQL insert %d subscribers by %d nanoseconds", limit, int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	if index {
		err = mysql.IndexFull(connection)
		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	logger.Infof("success complete by time %d nanoseconds", time.Now().Sub(start))
}
