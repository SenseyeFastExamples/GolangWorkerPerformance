package main

import (
	"flag"
	"fmt"
	"github.com/juju/errors"
	"time"
	"workerperformance/components/logger"
	"workerperformance/components/mysql"
)

func main() {
	helpFlag := flag.Bool("help", false, "help")
	resetFlag := flag.Bool("reset", false, "MySQL drop & create table")
	indexFlag := flag.Bool("index", false, "MySQL add index")
	shiftFlag := flag.Int("shift", 0, "shift")
	limitFlag := flag.Int("n", 0, "generate & fill MySQL table")

	flag.Parse()

	help := *helpFlag
	reset := *resetFlag
	index := *indexFlag
	shift := *shiftFlag
	limit := *limitFlag

	if help || index == false {
		flag.PrintDefaults()
		fmt.Println("example run command: fill -index")

		return
	}

	if reset || limit > 0 || shift > 0 {
		logger.Info("Please use MySQL filler for reset and fill table")

		return
	}

	start := time.Now()

	connection, err := mysql.Connection()
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
	defer connection.Close()

	if index {
		err := mysql.IndexWorker(connection)
		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	logger.Infof("success complete by time %d nanoseconds", time.Now().Sub(start))
}
