package stats

import (
	"math"
	"sync"
	"time"
)

type ResultStats struct {
	FastPercent   float64
	NormalPercent float64
	SlowPercent   float64
	Min           time.Duration
	Max           time.Duration
	Avg           time.Duration
	DocumentCount int
	N             int
}

type WorkerRange struct {
	fast   time.Duration
	normal time.Duration

	fastCount   int
	normalCount int
	slowCount   int

	documentCount int

	durations []time.Duration
}

func NewWorkerRange(size int, fast, normal time.Duration) *WorkerRange {
	return &WorkerRange{
		fast:   fast,
		normal: normal,

		fastCount:     0,
		normalCount:   0,
		slowCount:     0,
		documentCount: 0,

		durations: make([]time.Duration, 0, size),
	}
}

func (w *WorkerRange) Add(duration time.Duration, documentCount int) {
	w.durations = append(w.durations, duration)
	w.documentCount += documentCount

	if duration < w.fast {
		w.fastCount += 1
	} else if duration < w.normal {
		w.normalCount += 1
	} else {
		w.slowCount += 1
	}
}

type WorkerRangeSet struct {
	items []*WorkerRange
	mu    sync.Mutex
}

func NewRangeSet(size int) *WorkerRangeSet {
	return &WorkerRangeSet{
		items: make([]*WorkerRange, 0, size),
	}
}

func (r WorkerRange) All() int {
	return r.fastCount + r.normalCount + r.slowCount
}

func (s *WorkerRangeSet) Add(item *WorkerRange) {
	s.mu.Lock()
	s.items = append(s.items, item)
	s.mu.Unlock()
}

func (s *WorkerRangeSet) Stats() ResultStats {
	items := s.copy()

	if len(items) == 0 {
		return ResultStats{}
	}

	var (
		fastCount     = 0
		normalCount   = 0
		slowCount     = 0
		documentCount = 0
		n             = 0

		min   = time.Duration(math.MaxInt64)
		max   = time.Duration(0)
		total = time.Duration(0)
	)

	for _, item := range items {
		fastCount += item.fastCount
		normalCount += item.normalCount
		slowCount += item.slowCount

		documentCount += item.documentCount
		n += len(item.durations)

		for _, duration := range item.durations {
			total += duration

			if duration > max {
				max = duration
			}

			if duration < min {
				min = duration
			}
		}
	}

	all := fastCount + normalCount + slowCount
	if all == 0 {
		return ResultStats{}
	}

	return ResultStats{
		FastPercent:   float64(100 * fastCount / all),
		NormalPercent: float64(100 * normalCount / all),
		SlowPercent:   float64(100 * slowCount / all),
		Min:           min,
		Max:           max,
		Avg:           total / time.Duration(n),
		DocumentCount: documentCount,
		N:             n,
	}
}

func (s *WorkerRangeSet) copy() []*WorkerRange {
	s.mu.Lock()
	result := s.items[:]
	s.mu.Unlock()
	return result
}
