package scylladb

import (
	"github.com/gocql/gocql"
	"github.com/gopereza/packer"
	"github.com/juju/errors"
	"workerperformance/components/logger"
	"workerperformance/models/skylladb"
)

const (
	shortInsertSQL = "INSERT INTO subscribers (id, notify_after, worker_id, timezone, endpoint) VALUES (?, ?, ?, ?, ?);"
	fullInsertSQL  = "INSERT INTO subscribers (id, notify_after, partner, stream, widget, worker_id, timezone, os, browser, country, language, endpoint) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
	insertSQL      = shortInsertSQL
)

type FillHandle func(client *gocql.Session, subscribers []scylladb.Subscriber) error

func Fill(client *gocql.Session, subscribers []scylladb.Subscriber) error {
	batch := client.NewBatch(gocql.LoggedBatch)

	for _, subscriber := range subscribers {
		batch.Query(
			insertSQL,
			subscriber.ID,
			subscriber.NotifyAfter,
			//subscriber.Partner,
			//subscriber.Stream,
			//subscriber.Widget,
			subscriber.WorkerID,
			subscriber.Timezone,
			//subscriber.OS,
			//subscriber.Browser,
			//subscriber.Country,
			//subscriber.Language,
			subscriber.Endpoint,
		)
	}

	return client.ExecuteBatch(batch)
}

func FillBatch(client *gocql.Session, subscribers []scylladb.Subscriber) error {
	const batch = 25

	var lastErr error

	packs := packer.Pack(len(subscribers), batch)
	for _, pack := range packs {
		err := Fill(client, subscribers[pack.From:pack.To])

		if err != nil {
			lastErr = errors.Trace(err)

			logger.Error(lastErr)

			continue
		}
	}

	return lastErr
}

func FillBatchByPrepare(client *gocql.Session, subscribers []scylladb.Subscriber) error {
	stmt := client.Query(insertSQL)

	var lastErr error

	for _, subscriber := range subscribers {
		stmt.Bind(
			subscriber.ID,
			subscriber.NotifyAfter,
			//subscriber.Partner,
			//subscriber.Stream,
			//subscriber.Widget,
			subscriber.WorkerID,
			subscriber.Timezone,
			//subscriber.OS,
			//subscriber.Browser,
			//subscriber.Country,
			//subscriber.Language,
			subscriber.Endpoint,
		)

		err := stmt.Exec()
		if err != nil {
			lastErr = errors.Trace(err)

			logger.Error(lastErr)

			continue
		}
	}

	return lastErr
}

func Match(client *gocql.Session, workerID uint8, timezones []uint8, now uint32, limit int) ([]scylladb.Subscriber, error) {
	//sql := `
	//	SELECT id,
	//	       notify_after,
	//	       partner,
	//	       stream,
	//	       widget,
	//	       worker_id,
	//	       timezone,
	//	       os,
	//	       browser,
	//	       country,
	//	       language,
	//	       endpoint
	//	FROM subscribers_by_worker_timezone_notify
	//	WHERE worker_id = ?
	//	  AND timezone IN ?
	//	  AND notify_after <= ?
	//	LIMIT ?;
	//`

	sql := `
		SELECT id,
		       notify_after,
		       worker_id,
		       timezone,
		       endpoint
		FROM subscribers_by_worker_timezone_notify
		WHERE worker_id = ?
		  AND timezone IN ?
		  AND notify_after <= ?
		LIMIT ?;
	`

	query := client.Query(sql, workerID, timezones, now, limit)

	iterator := query.Iter()

	result := make([]scylladb.Subscriber, 0, limit)

	subscriber := scylladb.Subscriber{}

	for iterator.Scan(
		&subscriber.ID,
		&subscriber.NotifyAfter,
		//&subscriber.Partner,
		//&subscriber.Stream,
		//&subscriber.Widget,
		&subscriber.WorkerID,
		&subscriber.Timezone,
		//&subscriber.OS,
		//&subscriber.Browser,
		//&subscriber.Country,
		//&subscriber.Language,
		&subscriber.Endpoint,
	) {
		result = append(result, subscriber)
	}

	if err := iterator.Close(); err != nil {
		return nil, errors.Trace(err)
	}

	return result, nil
}

func Count(client *gocql.Session) (int, error) {
	sql := "SELECT COUNT(*) FROM subscribers;"

	query := client.Query(sql)

	result := 0

	err := query.Scan(&result)
	if err != nil {
		return 0, err
	}

	return result, nil
}

func Update(client *gocql.Session, ids []uint32, workerID, timezone uint8, notifyAfter uint32) error {
	query := client.Query(`
		UPDATE subscribers
		SET notify_after = ?
		WHERE id IN ?
		AND worker_id = ?
		AND timezone = ?;
	`,
		notifyAfter,
		ids,
		workerID,
		timezone,
	)

	return query.Exec()
}
