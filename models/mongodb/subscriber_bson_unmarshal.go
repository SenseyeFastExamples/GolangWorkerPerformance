package mongodb

import (
	"bytes"
	"github.com/juju/errors"
)

// http://bsonspec.org/spec.html
const (
	bsonInt32  = 0x10
	bsonString = 0x02
	end        = 0x00

	sbsonInt32  = "\x10"
	sbsonString = "\x02"
	send        = "\x00"
)

// first 4 bytes is document size
const (
	documentSize = 4
	int32Size    = 4
)

var (
	idColumn       = []byte(sbsonInt32 + "_id" + send)
	notifyColumn   = []byte(sbsonInt32 + "notify_after" + send)
	partnerColumn  = []byte(sbsonInt32 + "partner" + send)
	streamColumn   = []byte(sbsonInt32 + "stream" + send)
	widgetColumn   = []byte(sbsonInt32 + "widget" + send)
	workerColumn   = []byte(sbsonInt32 + "worker_id" + send)
	timezoneColumn = []byte(sbsonInt32 + "timezone" + send)
	osColumn       = []byte(sbsonInt32 + "os" + send)
	browserColumn  = []byte(sbsonInt32 + "browser" + send)
	countryColumn  = []byte(sbsonString + "country" + send)
	languageColumn = []byte(sbsonString + "language" + send)
	endpointColumn = []byte(sbsonString + "endpoint" + send)
)

func Unmarshal(data []byte, subscriber *Subscriber) error {
	if len(data) <= documentSize {
		return errors.New("document to small")
	}

	data = data[documentSize:]

	var err error

	subscriber.ID, data, err = nextInt32(data, idColumn, "_id")
	if err != nil {
		return err
	}

	subscriber.NotifyAfter, data, err = nextInt32(data, notifyColumn, "notify_after")
	if err != nil {
		return err
	}

	subscriber.Partner, data, err = nextInt32(data, partnerColumn, "partner")
	if err != nil {
		return err
	}

	subscriber.Stream, data, err = nextInt32(data, streamColumn, "stream")
	if err != nil {
		return err
	}

	subscriber.Widget, data, err = nextInt32(data, widgetColumn, "widget")
	if err != nil {
		return err
	}

	subscriber.WorkerID, data, err = nextInt8(data, workerColumn, "worker_id")
	if err != nil {
		return err
	}

	subscriber.Timezone, data, err = nextInt8(data, timezoneColumn, "timezone")
	if err != nil {
		return err
	}

	subscriber.OS, data, err = nextInt8(data, osColumn, "os")
	if err != nil {
		return err
	}

	subscriber.Browser, data, err = nextInt8(data, browserColumn, "browser")
	if err != nil {
		return err
	}

	subscriber.Country, data, err = nextString(data, countryColumn, "country")
	if err != nil {
		return err
	}

	subscriber.Language, data, err = nextString(data, languageColumn, "language")
	if err != nil {
		return err
	}

	subscriber.Endpoint, data, err = nextString(data, endpointColumn, "endpoint")
	if err != nil {
		return err
	}

	return nil
}

func nextInt32(data, column []byte, name string) (int32, []byte, error) {
	matchColumn := bytes.HasPrefix(data, column)
	if !matchColumn {
		return 0, nil, errors.Errorf("expect %s column", name)
	}

	data = data[len(column):]
	if len(data) < int32Size {
		return 0, nil, errors.Errorf("expect %s int32 value", name)
	}

	return bsonParseInt32(data[:int32Size]), data[int32Size:], nil
}

func nextInt8(data, column []byte, name string) (uint8, []byte, error) {
	matchColumn := bytes.HasPrefix(data, column)
	if !matchColumn {
		return 0, nil, errors.Errorf("expect %s column", name)
	}

	data = data[len(column):]
	if len(data) < int32Size {
		return 0, nil, errors.Errorf("expect %s int32 value", name)
	}

	return data[0], data[int32Size:], nil
}

func nextString(data, column []byte, name string) (string, []byte, error) {
	matchColumn := bytes.HasPrefix(data, column)
	if !matchColumn {
		return "", nil, errors.Errorf("expect %s column", name)
	}

	data = data[len(column):]
	if len(data) < int32Size {
		return "", nil, errors.Errorf("expect %s string size", name)
	}

	valueSize := int(bsonParseInt32(data[:int32Size]))
	data = data[int32Size:]

	if len(data) < valueSize {
		return "", nil, errors.Errorf("expect %s string value", name)
	}

	value := data[:valueSize]
	endIndex := valueSize - 1
	if value[endIndex] != end {
		return "", nil, errors.Errorf("expect %s string end", name)
	}

	result := string(data[:endIndex])

	return result, data[valueSize:], nil
}

func bsonParseInt32(bytes []byte) int32 {
	return int32(uint32(bytes[0]) | uint32(bytes[1])<<8 | uint32(bytes[2])<<16 | uint32(bytes[3])<<24)
}
