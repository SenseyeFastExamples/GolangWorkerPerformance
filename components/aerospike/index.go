package aerospike

import "github.com/aerospike/aerospike-client-go"

// https://www.aerospike.com/docs/tools/aql/index_management.html
// CREATE [indextype] INDEX <index> ON <ns>[.<set>] (<bin>) <type>

// SUCCESS:
// aql> SHOW INDEXES rtb.subscribers;
// aql> CREATE INDEX subscriber_worker ON rtb.subscribers (worker_id) NUMERIC;
// aql> CREATE INDEX subscriber_timezone ON rtb.subscribers (timezone) NUMERIC;
// aql> CREATE INDEX subscriber_notify ON rtb.subscribers (notify_after) NUMERIC;
// aql> SELECT * FROM rtb.subscribers WHERE worker_id = 1;
// aql> SELECT * FROM rtb.subscribers WHERE timezone = 1;
// aql> SELECT * FROM rtb.subscribers WHERE notify_after = 1;
// aql> SELECT * FROM rtb.subscribers WHERE timezone BETWEEN 0 AND 23;
// aql> SELECT * FROM rtb.subscribers WHERE worker_id = 1 AND timezone = 1 AND notify_after = 1;

// DROP INDEX <ns> <index>
// aql> DROP INDEX rtb.subscribers subscriber_worker;
// aql> DROP INDEX rtb.subscribers subscriber_timezone;
// aql> DROP INDEX rtb.subscribers subscriber_notify;

// FAILED:
// aql> SELECT * FROM rtb.subscribers WHERE worker_id >= 1;
// aql> SELECT * FROM rtb.subscribers WHERE timezone >= 1;
// aql> SELECT * FROM rtb.subscribers WHERE notify_after >= 1;

func Index(client *aerospike.Client) error {
	_, err := client.CreateIndex(nil, Namespace, SetName, "worker_timezone_notify_bitset_index", workerTimezoneNotifyMergeBin, aerospike.NUMERIC)

	return err
}
