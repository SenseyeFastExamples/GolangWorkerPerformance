up:
	sudo docker-compose up -d

build:
	sudo docker-compose up -d --build

app:
	sudo docker exec -it workerperformance_app sh

mongodb:
	sudo docker exec -it workerperformance_mongodb bash

redis:
	sudo docker exec -it workerperformance_redis bash

dynomitedb-redis:
	sudo docker exec -it workerperformance_dynomitedb-redis bash

scylladb:
	sudo docker exec -it workerperformance_scylladb-node-1 bash

scylladb-keyspace:
	sudo docker exec workerperformance_scylladb-node-1 cqlsh -e "CREATE KEYSPACE rtb WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 2}" --request-timeout=3600

scylladb-count:
	sudo docker exec workerperformance_scylladb-node-1 cqlsh -e "SELECT COUNT(*) FROM rtb.subscribers" --request-timeout=3600

aerospike:
	sudo docker exec -it workerperformance_aerospike bash

mysql:
	sudo docker exec -it workerperformance_mysql bash

down:
	sudo docker-compose down

proto:
	protoc -I . protos/redis/*.proto --go_out=models

binaries:
	go build -o /bin/mongodb-fixtures-filler ./development/mongodb/command/fixtures/
	go build -o /bin/mongodb-worker ./development/mongodb/command/worker/
	go build -o /bin/scylladb-fixtures-filler ./development/scylladb/command/fixtures/
	go build -o /bin/scylladb-worker ./development/scylladb/command/worker/
	go build -o /bin/redis-fixtures-filler ./development/redis/command/fixtures/
	go build -o /bin/redis-worker ./development/redis/command/worker/