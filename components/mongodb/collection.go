package mongodb

import (
	"context"
	"github.com/juju/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"sync"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
	"workerperformance/models/mongodb"
)

func Collection(client *mongo.Client) *mongo.Collection {
	return client.Database("rtb").Collection("subscriber")
}

func Index(collection *mongo.Collection) error {
	asc := bsonx.Int32(1)
	desc := bsonx.Int32(-1)

	_, err := collection.Indexes().CreateMany(context.TODO(), []mongo.IndexModel{
		{
			Keys: bsonx.Doc{
				{"worker_id", asc},
				{"timezone", asc},
				{"notify_after", desc},
			},
		},
	})

	return err
}

func Drop(collection *mongo.Collection) error {
	return collection.Drop(context.TODO())
}

func Fill(collection *mongo.Collection, subscribers []mongodb.Subscriber) error {
	documents := make([]interface{}, len(subscribers))

	for i, subscriber := range subscribers {
		documents[i] = subscriber
	}

	_, err := collection.InsertMany(context.TODO(), documents, nil)

	return err
}

func FillBatch(collection *mongo.Collection, subscribers []mongodb.Subscriber) error {
	length := len(subscribers)

	if length > consts.Batch {
		index := 0
		documents := make([]interface{}, 0, consts.Batch)

		for i := 0; i < length; i++ {
			documents = append(documents, subscribers[i])
			index++

			if index >= consts.Batch {
				_, err := collection.InsertMany(context.TODO(), documents, nil)

				if err != nil {
					return err
				}

				index = 0
				documents = documents[:0]
			}
		}

		if index > 0 {
			_, err := collection.InsertMany(context.TODO(), documents, nil)

			if err != nil {
				return err
			}
		}

		return nil
	}

	return Fill(collection, subscribers)
}

func FillBatchParallel(subscribers []mongodb.Subscriber) error {
	length := len(subscribers)

	if length > consts.Batch {
		wg := new(sync.WaitGroup)

		packs := length / consts.Batch
		end := length % consts.Batch

		parallel := packs
		if end > 0 {
			parallel += 1
		}
		wg.Add(parallel)

		logger.Infof("fill batch by %d goroutines", parallel)

		clients := make([]*mongo.Client, parallel)
		for i := 0; i < parallel; i++ {
			client, err := Client()
			if err != nil {
				return errors.Trace(err)
			}
			clients[i] = client
			defer client.Disconnect(context.TODO())
		}

		for i := 0; i < packs; i++ {
			go func(i int) {
				from := i * consts.Batch
				to := from + consts.Batch

				err := Fill(Collection(clients[i]), subscribers[from:to])

				logger.Error(errors.Trace(err))

				wg.Done()
			}(i)
		}

		if end > 0 {
			go func() {
				err := Fill(Collection(clients[packs]), subscribers[packs*consts.Batch:])

				logger.Error(errors.Trace(err))

				wg.Done()
			}()
		}

		wg.Wait()

		return nil
	}

	client, err := Client()
	if err != nil {
		return errors.Trace(err)
	}
	defer client.Disconnect(context.TODO())

	return Fill(Collection(client), subscribers)
}
