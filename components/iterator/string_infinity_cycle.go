package iterator

type StringInfinityCycle struct {
	data   []string
	cursor int
	length int
}

func NewStringInfinityCycle(data []string) *StringInfinityCycle {
	return &StringInfinityCycle{
		data:   data,
		cursor: 0,
		length: len(data),
	}
}

func (u *StringInfinityCycle) Next() string {
	u.cursor = (u.cursor + 1) % u.length

	return u.data[u.cursor]
}

func (u *StringInfinityCycle) ValueByIndex(index int) string {
	return u.data[index%u.length]
}
