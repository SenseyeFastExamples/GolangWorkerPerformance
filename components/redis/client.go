package redis

import (
	"github.com/go-redis/redis"
	"workerperformance/components/env"
)

func ClientByNode(int) (*redis.Client, error) {
	return Client()
}

func Client() (*redis.Client, error) {
	return ClientByURI(env.RedisURI())
}

func ClientByURI(uri string) (*redis.Client, error) {
	credentials := &redis.Options{
		Addr:     uri,
		Password: "", // no password set
		DB:       0,  // use default DB
	}

	client := redis.NewClient(credentials)

	pong, err := client.Ping().Result()
	_ = pong // PONG
	if err != nil {
		return nil, err
	}
	return client, nil
}
