package native

import (
	"database/sql"
	"workerperformance/components/mysql"
	models "workerperformance/models/mysql"
)

const (
	nextQuery = `
SELECT id, notify_after, partner, stream, widget, worker_id, timezone, os, browser, country, language, endpoint
FROM subscribers
WHERE id > ?
AND worker_id = ?
ORDER BY id ASC
LIMIT ?
`
)

func Next(connection *sql.DB, workerID uint8, afterID uint32, limit uint) ([]models.Subscriber, error) {
	rows, err := connection.Query(nextQuery, afterID, workerID, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]models.Subscriber, 0, limit)
	for rows.Next() {
		subscriber := models.Subscriber{}

		err = rows.Scan(
			&subscriber.ID,
			&subscriber.NotifyAfter,
			&subscriber.Partner,
			&subscriber.Stream,
			&subscriber.Widget,
			&subscriber.WorkerID,
			&subscriber.Timezone,
			&subscriber.OS,
			&subscriber.Browser,
			&subscriber.Country,
			&subscriber.Language,
			&subscriber.Endpoint,
		)
		if err != nil {
			return nil, err
		}

		result = append(result, subscriber)
	}

	return result, nil
}

func update(connection *sql.DB, ids []uint32, now uint32) error {
	return mysql.Update(connection, ids, now)
}
