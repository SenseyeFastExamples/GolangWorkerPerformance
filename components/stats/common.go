package stats

import (
	"math"
	"time"
	"workerperformance/components/logger"
)

const (
	MatchFast   = 150 * time.Millisecond
	MatchNormal = 250 * time.Millisecond

	UpdateFast   = 500 * time.Millisecond
	UpdateNormal = 1000 * time.Millisecond
)

const (
	intervalFetchPattern  = "fetch:  0-150 %6.2f%%; 150-250  %6.2f%%;  250+ %6.2f%%; avg %5.2f; max %5.2f; min %5.2f; documents %9d; N %5d"
	intervalUpdatePattern = "update: 0-500 %6.2f%%; 500-1000 %6.2f%%; 1000+ %6.2f%%; avg %5.2f; max %5.2f; min %5.2f; documents %9d; N %5d"
)

func IntervalStatsFetch(set *WorkerRangeSet) {
	renderByPattern(set, intervalFetchPattern)
}

func IntervalStatsUpdate(set *WorkerRangeSet) {
	renderByPattern(set, intervalUpdatePattern)
}

func renderByPattern(set *WorkerRangeSet, pattern string) {
	stats := set.Stats()

	logger.Infof(
		pattern,
		stats.FastPercent, stats.NormalPercent, stats.SlowPercent,
		ToS(stats.Avg), ToS(stats.Max), ToS(stats.Min),
		stats.DocumentCount, stats.N,
	)
}

func ToS(duration time.Duration) float64 {
	return math.Ceil(float64(duration/1e6)) / 1e3
}
