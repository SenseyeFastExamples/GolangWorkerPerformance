package oplogger

import (
	"strconv"
	"sync"
)

const (
	match  = 1
	update = 2
)

type log struct {
	time      int32
	workerID  int
	iteration int
	length    int
	duration  int64
	operation uint8
	success   bool
}

type Logger struct {
	logs            []log
	workerMatchMap  map[int][]log
	workerUpdateMap map[int][]log
	mu              *sync.Mutex
}

func NewLogger(size int) *Logger {
	return &Logger{
		logs:            make([]log, 0, size),
		workerMatchMap:  make(map[int][]log, size),
		workerUpdateMap: make(map[int][]log, size),
		mu:              new(sync.Mutex),
	}
}

func (l *Logger) LogMatch(now int32, workerID, iteration, length int, duration int64, success bool) {
	l.log(now, workerID, iteration, length, duration, match, success)
}

func (l *Logger) LogUpdate(now int32, workerID, iteration, length int, duration int64, success bool) {
	l.log(now, workerID, iteration, length, duration, update, success)
}

func (l *Logger) Dump() []byte {
	const rowSize = 64

	result := make([]byte, 0, len(l.logs)*rowSize)
	for _, log := range l.logs {
		result = strconv.AppendUint(result, uint64(log.time), 10)
		result = append(result, ',')
		result = strconv.AppendUint(result, uint64(log.workerID), 10)
		result = append(result, ',')
		result = strconv.AppendUint(result, uint64(log.iteration), 10)
		result = append(result, ',')
		result = strconv.AppendUint(result, uint64(log.length), 10)
		result = append(result, ',')
		result = strconv.AppendUint(result, uint64(log.duration), 10)
		result = append(result, ',')

		if log.operation == match {
			result = append(result, "match"...)
		} else {
			result = append(result, "update"...)
		}
		result = append(result, ',')

		if log.success {
			result = append(result, "success"...)
		} else {
			result = append(result, "fail"...)
		}
		result = append(result, '\n')
	}

	return result
}

func (l *Logger) log(now int32, workerID, iteration, length int, duration int64, operation uint8, success bool) {
	l.mu.Lock()
	data := log{
		time:      now,
		workerID:  workerID,
		iteration: iteration,
		length:    length,
		duration:  duration,
		operation: operation,
		success:   success,
	}
	l.logs = append(l.logs, data)

	switch operation {
	case match:
		l.workerMatchMap[workerID] = append(l.workerMatchMap[workerID], data)
	case update:
		l.workerUpdateMap[workerID] = append(l.workerUpdateMap[workerID], data)
	}

	l.mu.Unlock()
}
