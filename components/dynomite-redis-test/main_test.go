package test

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"workerperformance/components/consts"
	dynomiteredis "workerperformance/components/dynomite-redis"
	"workerperformance/components/etalon"
	"workerperformance/components/redis"
	"workerperformance/components/redis-test"
)

var (
	once = new(sync.Once)
)

func TestMatch(t *testing.T) {
	client, err := dynomiteredis.Client()
	if !assert.NoError(t, err) {
		return
	}
	defer client.Close()

	err = dynomiteredis.FlushDB(client)
	if !assert.NoError(t, err) {
		return
	}

	test.CommonTestMatch(t, client, redis.StoreKeys, redis.MatchKeys)
}

func BenchmarkStore(b *testing.B) {
	client, err := dynomiteredis.Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	err = dynomiteredis.FlushDB(client)
	if !assert.NoError(b, err) {
		return
	}

	test.CommonBenchmarkStore(b, client, redis.StoreKeys)
}

func BenchmarkMatch(b *testing.B) {
	client, err := dynomiteredis.Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	// to fill many documents once by benchmark 1, 10, ... 1 000 000
	once.Do(func() {
		err = dynomiteredis.FlushDB(client)
		if err != nil {
			return
		}

		subscribers := etalon.Generate(0, consts.WorkerCount, consts.TimezoneCount, 1e6)

		_, err = redis.StoreKeys(client, subscribers)
		if err != nil {
			return
		}
	})

	if !assert.NoError(b, err) {
		return
	}

	test.CommonBenchmarkMatch(b, client, redis.MatchKeys)
}
