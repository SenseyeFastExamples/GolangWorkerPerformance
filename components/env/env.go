package env

import (
	"os"
	"strings"
)

func MongoDBURI() string {
	return os.Getenv("MONGODB_URI")
}

func RedisURI() string {
	return os.Getenv("REDIS_URI")
}

func DynomiteRedisURI() []string {
	source := os.Getenv("DYNOMITE_REDIS_URI")

	if source == "" {
		return nil
	}

	return strings.Split(source, ",")
}

func ScyllaDBURIs() []string {
	source := os.Getenv("SCYLLADB_URIS")

	if source == "" {
		return nil
	}

	return strings.Split(source, ",")
}
