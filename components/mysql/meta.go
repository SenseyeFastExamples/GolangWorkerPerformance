package mysql

const (
	subscribersTableSimpleCreate = `
CREATE TABLE subscribers
(
  id           INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  notify_after INT(11) UNSIGNED    NOT NULL,
  partner      INT(11) UNSIGNED    NOT NULL,
  stream       INT(11) UNSIGNED    NOT NULL,
  widget       INT(11) UNSIGNED    NOT NULL,
  worker_id    TINYINT(3) UNSIGNED NOT NULL,
  timezone     TINYINT(3) UNSIGNED NOT NULL,
  os           TINYINT(3) UNSIGNED NOT NULL,
  browser      TINYINT(3) UNSIGNED NOT NULL,
  country      CHAR(2)             NOT NULL,
  language     CHAR(2)             NOT NULL,
  endpoint     VARCHAR(255)        NOT NULL
) ENGINE = InnoDB;
`

	subscribersTablePartitionCreate = `
CREATE TABLE subscribers
(
  id           INT(11) UNSIGNED AUTO_INCREMENT,
  notify_after INT(11) UNSIGNED    NOT NULL,
  partner      INT(11) UNSIGNED    NOT NULL,
  stream       INT(11) UNSIGNED    NOT NULL,
  widget       INT(11) UNSIGNED    NOT NULL,
  worker_id    TINYINT(3) UNSIGNED NOT NULL,
  timezone     TINYINT(3) UNSIGNED NOT NULL,
  os           TINYINT(3) UNSIGNED NOT NULL,
  browser      TINYINT(3) UNSIGNED NOT NULL,
  country      CHAR(2)             NOT NULL,
  language     CHAR(2)             NOT NULL,
  endpoint     VARCHAR(255)        NOT NULL
  PRIMARY KEY (id, worker_id)
) ENGINE = InnoDB
  PARTITION BY HASH (worker_id)
    PARTITIONS 20;
`

	subscribersTableCreate = subscribersTableSimpleCreate

	subscribersTableDrop = `
DROP TABLE IF EXISTS subscribers;
`

	subscribersFullIndex = `
ALTER TABLE subscribers
  ADD INDEX WORKER_TIMEZONE_NOTIFY_INDEX (worker_id, timezone, notify_after DESC);
`

	subscribersWorkerIndex = `
ALTER TABLE subscribers
  ADD INDEX WORKER_INDEX (worker_id);
`
)
