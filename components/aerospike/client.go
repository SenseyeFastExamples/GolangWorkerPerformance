package aerospike

import (
	"github.com/aerospike/aerospike-client-go"
	settings "workerperformance/models/aerospike"
)

var (
	credentials = settings.Credentials{
		Hostname: "aerospike",
		Port:     3000,
	}
)

func Client() (*aerospike.Client, error) {
	return aerospike.NewClient(credentials.Hostname, credentials.Port)
}
