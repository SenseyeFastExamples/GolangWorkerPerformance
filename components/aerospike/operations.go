package aerospike

import (
	"github.com/aerospike/aerospike-client-go"
	"github.com/juju/errors"
	"workerperformance/components/logger"
	models "workerperformance/models/aerospike"
)

func Store(client *aerospike.Client, subscriber models.Subscriber) error {
	return client.PutBins(
		nil,
		key(subscriber.ID),
		&aerospike.Bin{
			Name:  idBin,
			Value: aerospike.NewIntegerValue(int(subscriber.ID)),
		},
		&aerospike.Bin{
			Name:  workerTimezoneNotifyMergeBin,
			Value: aerospike.NewIntegerValue(int(subscriber.Marshal())),
		},
		&aerospike.Bin{
			Name:  countryBin,
			Value: aerospike.NewStringValue(subscriber.Country),
		},
		&aerospike.Bin{
			Name:  languageBin,
			Value: aerospike.NewStringValue(subscriber.Language),
		},
		&aerospike.Bin{
			Name:  endpointBin,
			Value: aerospike.NewStringValue(subscriber.Endpoint),
		},
	)
}

func StoreBatch(client *aerospike.Client, subscribers []models.Subscriber) error {
	length := len(subscribers)

	for i := 0; i < length; i++ {
		err := Store(client, subscribers[i])

		if err != nil {
			return err
		}
	}

	return nil
}

func fetch(client *aerospike.Client, subscriberID uint32) (models.Subscriber, error) {
	key := key(subscriberID)

	record, err := client.Get(nil, key)

	if err != nil {
		return models.Subscriber{}, errors.Trace(err)
	}

	result := parseRecord(subscriberID, record)

	return result, nil
}

func Match(client *aerospike.Client, workerID, timezone uint8, notifyAfter uint32, limit int) ([]models.Subscriber, error) {
	from := models.WorkerTimezoneNotifyMarshal(workerID, timezone, 0)
	to := models.WorkerTimezoneNotifyMarshal(workerID, timezone, notifyAfter)

	// https://github.com/aerospike/aerospike-client-go/blob/f257953b1650505cf4c357fcc4f032d160ebb07e/client_object_test.go#L1107
	statement := aerospike.NewStatement(Namespace, SetName)
	statement.SetFilter(aerospike.NewRangeFilter(workerTimezoneNotifyMergeBin, from, to))

	rs, err := client.Query(nil, statement)
	if err != nil {
		return nil, err
	}
	defer rs.Close()

	result := make([]models.Subscriber, 0, limit)
	index := 0
	for item := range rs.Results() {
		err := item.Err
		if err != nil {
			logger.Error(errors.Trace(err))

			continue
		}

		record := item.Record
		id := uint32(parseInteger(record.Bins, idBin))

		result = append(result, parseRecord(id, record))

		index++
		if index >= limit {
			break
		}
	}

	return result, nil
}

func parseRecord(subscriberID uint32, record *aerospike.Record) models.Subscriber {
	result := models.Subscriber{
		ID: subscriberID,
	}

	result.Unmarshal(int64(parseInteger(record.Bins, workerTimezoneNotifyMergeBin)))

	result.Country = parseString(record.Bins, countryBin)
	result.Language = parseString(record.Bins, languageBin)
	result.Endpoint = parseString(record.Bins, endpointBin)

	return result
}

func parseInteger(binMap aerospike.BinMap, binName string) int {
	if value, ok := binMap[binName]; ok {
		if value, ok := value.(int); ok {
			return value
		}
	}

	return 0
}

func parseString(binMap aerospike.BinMap, binName string) string {
	if value, ok := binMap[binName]; ok {
		if value, ok := value.(string); ok {
			return value
		}
	}

	return ""
}
