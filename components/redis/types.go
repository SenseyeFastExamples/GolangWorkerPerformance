package redis

import (
	"github.com/go-redis/redis"
	models "workerperformance/models/redis"
)

type (
	StoreHandle func(client *redis.Client, subscribers []models.Subscriber) (int, error)
	MatchHandle func(client *redis.Client, workerID uint8, timezone uint8, now uint32, limit int) ([]models.Subscriber, error)
	FlushHandle func(client *redis.Client) error
)
