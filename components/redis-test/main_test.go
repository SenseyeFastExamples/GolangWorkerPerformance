package test

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"workerperformance/components/consts"
	"workerperformance/components/etalon"
	"workerperformance/components/redis"
)

var (
	once = new(sync.Once)
)

func TestMatch(t *testing.T) {
	client, err := redis.Client()
	if !assert.NoError(t, err) {
		return
	}
	defer client.Close()

	cmd := client.FlushDB()
	if !assert.NoError(t, cmd.Err()) {
		return
	}

	CommonTestMatch(t, client, redis.StoreShardHashes, redis.MatchShardHashes)
}

func BenchmarkStore(b *testing.B) {
	client, err := redis.Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	cmd := client.FlushDB()
	if !assert.NoError(b, cmd.Err()) {
		return
	}

	CommonBenchmarkStore(b, client, redis.StoreShardHashes)
}

/**
1e5:
BenchmarkMatch   	   30000	    962512 ns/op	  212295 B/op	    2494 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:204: match count 1 of b.N = 1, match document count 213 of 8000
    main_test.go:204: match count 100 of b.N = 100, match document count 20375 of 800000
    main_test.go:204: match count 10000 of b.N = 10000, match document count 2028425 of 80000000
    main_test.go:204: match count 30000 of b.N = 30000, match document count 6085000 of 240000000
PASS
ok  	workerperformance/components/redis	39.191s
1e6:
BenchmarkMatch   	    3000	   8277857 ns/op	 2167039 B/op	   25002 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:215: match count 1 of b.N = 1, match document count 2096 of 8000
    main_test.go:215: match count 100 of b.N = 100, match document count 207878 of 800000
    main_test.go:215: match count 3000 of b.N = 3000, match document count 6235600 of 24000000
PASS
ok  	workerperformance/components/redis	30.144s
*/
func BenchmarkMatch(b *testing.B) {
	client, err := redis.Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	// to fill many documents once by benchmark 1, 10, ... 1 000 000
	once.Do(func() {
		err = client.FlushDB().Err()
		if err != nil {
			return
		}

		subscribers := etalon.Generate(0, consts.WorkerCount, consts.TimezoneCount, 1e6)

		_, err = redis.StoreShardHashes(client, subscribers)
		if err != nil {
			return
		}
	})

	if !assert.NoError(b, err) {
		return
	}

	CommonBenchmarkMatch(b, client, redis.MatchShardHashes)
}

/**
BenchmarkStoreOne   	   10000	   7260651 ns/op	 2002730 B/op	   20040 allocs/op
PASS
ok  	workerperformance/components/redis	72.654s
*/
func BenchmarkStoreOne(b *testing.B) {
	b.Skip()

	client, err := redis.Client()
	if !assert.NoError(b, err) {
		return
	}
	defer client.Close()

	cmd := client.FlushDB()
	if !assert.NoError(b, cmd.Err()) {
		return
	}

	subscribers := etalon.Generate(0, 1, 1, b.N)
	for i := 0; i < b.N; i++ {
		_, _ = redis.StoreShardHashes(client, subscribers)
	}
}
