package test

import (
	goredis "github.com/go-redis/redis"
	"github.com/stretchr/testify/assert"
	"testing"
	"workerperformance/components/consts"
	"workerperformance/components/etalon"
	"workerperformance/components/redis"
	"workerperformance/components/roundtime"
	models "workerperformance/models/redis"
)

func CommonTestMatch(t *testing.T, client *goredis.Client, store redis.StoreHandle, match redis.MatchHandle) {
	t.Helper()

	const hour = 3600
	now := roundtime.Now()
	before1h := now - hour
	after1h := now + hour

	w1t1e1 := models.Subscriber{
		ID:          1,
		NotifyAfter: now,
		Partner:     1,
		Stream:      1,
		Widget:      1,
		OS:          1,
		Browser:     1,
		WorkerID:    1,
		Timezone:    1,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    consts.Endpoint,
	}
	w1t1e2 := models.Subscriber{
		ID:          2,
		NotifyAfter: now,
		Partner:     2,
		Stream:      2,
		Widget:      2,
		OS:          2,
		Browser:     2,
		WorkerID:    1,
		Timezone:    1,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    consts.Endpoint,
	}
	w1t1e3 := models.Subscriber{
		ID:          3,
		NotifyAfter: before1h,
		Partner:     3,
		Stream:      3,
		Widget:      3,
		OS:          3,
		Browser:     3,
		WorkerID:    1,
		Timezone:    1,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    consts.Endpoint,
	}
	subscribers := []models.Subscriber{
		// matched
		w1t1e1,
		// matched
		w1t1e2,
		// matched
		w1t1e3,
		// skipped
		{
			ID:          4,
			WorkerID:    1,
			Timezone:    1,
			NotifyAfter: after1h,
		},
		// skipped
		{
			ID:          5,
			WorkerID:    1,
			Timezone:    2,
			NotifyAfter: now,
		},
		// skipped
		{
			ID:          6,
			WorkerID:    2,
			Timezone:    1,
			NotifyAfter: now,
		},
	}

	_, err := store(client, subscribers)
	if !assert.NoError(t, err) {
		return
	}

	// sorted by score ASC
	expect := []models.Subscriber{w1t1e3, w1t1e1, w1t1e2}

	actual, err := match(client, 1, 1, now, 3)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)

	_, err = redis.Update(client, []models.Subscriber{w1t1e1, w1t1e3}, after1h)
	if !assert.NoError(t, err) {
		return
	}

	expect = []models.Subscriber{w1t1e2}

	// match after update
	actual, err = match(client, 1, 1, now, 10)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)
}

func CommonBenchmarkStore(b *testing.B, client *goredis.Client, store redis.StoreHandle) {
	b.Helper()

	subscribers := etalon.Generate(0, consts.WorkerCount, consts.TimezoneCount, b.N)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = store(client, subscribers)
	}
}

func CommonBenchmarkMatch(b *testing.B, client *goredis.Client, match redis.MatchHandle) {
	b.Helper()

	now := roundtime.Now()

	matchCount := 0
	matchDocumentCount := 0

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		result, err := match(client, uint8(i%consts.WorkerCount), uint8(i*i%consts.TimezoneCount), now, consts.Limit)
		documentCount := len(result)

		assert.NoError(b, err)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N)
}
