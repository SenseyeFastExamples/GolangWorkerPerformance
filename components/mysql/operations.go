package mysql

import (
	"database/sql"
	"strconv"
	"workerperformance/components/consts"
	"workerperformance/models/mysql"
)

var credentials = mysql.Credentials{
	User: "user",
	Pass: "pass",
	Host: "mysql",
	Port: "3306",
	Name: "rtb",
}

func Connection() (*sql.DB, error) {
	return connect(credentials)
}

func Reset(connection *sql.DB) error {
	var err error

	_, err = connection.Exec(subscribersTableDrop)
	if err != nil {
		return err
	}

	_, err = connection.Exec(subscribersTableCreate)
	if err != nil {
		return err
	}

	return nil
}

func tableCount(connection *sql.DB) (int, error) {
	rows, err := connection.Query("SELECT COUNT(*) FROM subscribers;")
	if err != nil {
		return 0, err
	}
	defer rows.Close()

	result := 0
	for rows.Next() {
		err := rows.Scan(&result)

		if err != nil {
			return 0, err
		}
	}

	err = rows.Err()
	if err != nil {
		return 0, err
	}

	return result, nil
}

func IndexFull(connection *sql.DB) error {
	_, err := connection.Exec(subscribersFullIndex)

	return err
}

func IndexWorker(connection *sql.DB) error {
	_, err := connection.Exec(subscribersWorkerIndex)

	return err
}

func Match(connection *sql.DB, workerID uint8, timezones []int, now uint32, limit uint) ([]mysql.Subscriber, error) {
	rows, err := connection.Query(matchQuery(workerID, timezones, now, limit))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]mysql.Subscriber, 0, limit)
	for rows.Next() {
		subscriber := mysql.Subscriber{}

		err = rows.Scan(
			&subscriber.ID,
			&subscriber.NotifyAfter,
			&subscriber.Partner,
			&subscriber.Stream,
			&subscriber.Widget,
			&subscriber.WorkerID,
			&subscriber.Timezone,
			&subscriber.OS,
			&subscriber.Browser,
			&subscriber.Country,
			&subscriber.Language,
			&subscriber.Endpoint,
		)
		if err != nil {
			return nil, err
		}

		result = append(result, subscriber)
	}

	return result, nil
}

func Update(connection *sql.DB, ids []uint32, now uint32) error {
	if len(ids) == 0 {
		return nil
	}

	_, err := connection.Exec(updateQuery(ids, now))

	return err
}

func UpdateWorker(connection *sql.DB, workerID uint8, ids []uint32, now uint32) error {
	if len(ids) == 0 {
		return nil
	}

	_, err := connection.Exec(updateWorkerQuery(workerID, ids, now))

	return err
}

func matchQuery(workerID uint8, timezones []int, now uint32, limit uint) string {
	const (
		uint32Size         = 10
		header             = `SELECT id, notify_after, partner, stream, widget, worker_id, timezone, os, browser, country, language, endpoint FROM subscribers`
		whereWorker        = "\nWHERE worker_id = "
		whereTimezoneStart = "\nAND timezone IN ("
		whereTimezoneEnd   = ")"
		whereNotify        = "\nAND notify_after <= "
		andLimit           = "\nLIMIT "
		end                = ";"
	)

	timezonesLength := len(timezones)
	commas := timezonesLength - 1

	size := len(header) +
		len(whereWorker) + uint32Size +
		len(whereTimezoneStart) + timezonesLength*uint32Size + commas + len(whereTimezoneEnd) +
		len(whereNotify) + uint32Size +
		len(andLimit) + uint32Size + len(end)

	query := make([]byte, 0, size)
	query = append(query, header...)
	query = append(query, whereWorker...)
	query = strconv.AppendUint(query, uint64(workerID), 10)
	query = append(query, whereTimezoneStart...)
	query = appendINint(query, timezones, commas)
	query = append(query, whereTimezoneEnd...)
	query = append(query, whereNotify...)
	query = strconv.AppendUint(query, uint64(now), 10)
	query = append(query, andLimit...)
	query = strconv.AppendUint(query, uint64(limit), 10)
	query = append(query, end...)

	return string(query)
}

func updateQuery(ids []uint32, now uint32) string {
	// https://stackoverflow.com/questions/45351644/golang-slice-in-mysql-query-with-where-in-clause?rq=1
	const (
		uint32Size = 10
		header     = "UPDATE subscribers"
		set        = "\nSET notify_after = "
		where      = "\nWHERE id IN ("
		end        = ");"
	)

	idsLength := len(ids)
	commas := idsLength - 1

	size := len(header) +
		len(set) + uint32Size +
		len(where) + idsLength*uint32Size + commas + len(end)

	query := make([]byte, 0, size)
	query = append(query, header...)
	query = append(query, set...)
	query = strconv.AppendUint(query, uint64(now), 10)
	query = append(query, where...)

	query = appendINuint32(query, ids, commas)

	query = append(query, end...)

	return string(query)
}

func updateWorkerQuery(workerID uint8, ids []uint32, now uint32) string {
	const (
		uint32Size = 10
		header     = "UPDATE subscribers"
		set        = "\nSET notify_after = "
		where      = "\nWHERE worker_id = "
		and        = "\nAND id IN ("
		end        = ");"
	)

	idsLength := len(ids)
	commas := idsLength - 1

	size := len(header) +
		len(set) + uint32Size +
		len(where) + uint32Size +
		len(and) + idsLength*uint32Size + commas + len(end)

	query := make([]byte, 0, size)
	query = append(query, header...)
	query = append(query, set...)
	query = strconv.AppendUint(query, uint64(now), 10)
	query = append(query, where...)
	query = strconv.AppendUint(query, uint64(workerID), 10)
	query = append(query, and...)

	query = appendINuint32(query, ids, commas)

	query = append(query, end...)

	return string(query)
}

func appendINuint32(dst []byte, ids []uint32, commas int) []byte {
	for i := 0; i < commas; i++ {
		dst = strconv.AppendUint(dst, uint64(ids[i]), 10)
		dst = append(dst, ',')
	}
	dst = strconv.AppendUint(dst, uint64(ids[commas]), 10)

	return dst
}

func appendINint(dst []byte, ids []int, commas int) []byte {
	for i := 0; i < commas; i++ {
		dst = strconv.AppendUint(dst, uint64(ids[i]), 10)
		dst = append(dst, ',')
	}
	dst = strconv.AppendUint(dst, uint64(ids[commas]), 10)

	return dst
}

func Fill(connection *sql.DB, subscribers []mysql.Subscriber) error {
	transaction, err := connection.Begin()
	if err != nil {
		return err
	}
	defer transaction.Commit()

	stmt, err := transaction.Prepare(`INSERT INTO subscribers (id, notify_after, partner, stream, widget, worker_id, timezone, os, browser, country, language, endpoint) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`)

	if err != nil {
		return err
	}

	for _, subscriber := range subscribers {
		_, err = stmt.Exec(
			subscriber.ID,
			subscriber.NotifyAfter,
			subscriber.Partner,
			subscriber.Stream,
			subscriber.Widget,
			subscriber.WorkerID,
			subscriber.Timezone,
			subscriber.OS,
			subscriber.Browser,
			subscriber.Country,
			subscriber.Language,
			subscriber.Endpoint,
		)

		if err != nil {
			return err
		}
	}

	return nil
}

func FillBatch(connection *sql.DB, subscribers []mysql.Subscriber) error {
	length := len(subscribers)
	if length <= consts.Batch {
		return Fill(connection, subscribers)
	}

	from := 0
	to := int(consts.Batch)
	for to <= length {
		err := Fill(connection, subscribers[from:to])

		if err != nil {
			return err
		}

		from, to = to, to+consts.Batch
	}

	if from < length {
		return Fill(connection, subscribers[from:length])
	}

	return nil
}
