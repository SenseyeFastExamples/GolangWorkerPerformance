package mysql

type Credentials struct {
	User string
	Pass string
	Host string
	Port string
	Name string
}
