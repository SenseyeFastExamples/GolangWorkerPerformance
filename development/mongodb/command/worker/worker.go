package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/juju/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"math"
	"sync"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
	"workerperformance/components/mongodb"
	"workerperformance/components/stats"
)

func main() {
	var (
		helpFlag       = flag.Bool("help", false, "help")
		batchFlag      = flag.Uint("batch", 0, "subscribers fetch limit")
		notifyTimeFlag = flag.Uint("notify_time", 0, "seconds sleep to emulate send")
		restTimeFlag   = flag.Uint("rest_time", 0, "seconds sleep after send")
		updateTimeFlag = flag.Uint("update_time", 0, "seconds when subscribers pack will available to next send")
		nFlag          = flag.Uint("n", 0, "iterations")
	)

	flag.Parse()

	var (
		help       = *helpFlag
		batch      = *batchFlag
		notifyTime = *notifyTimeFlag
		restTime   = *restTimeFlag
		updateTime = *updateTimeFlag
		n          = *nFlag
	)

	if help || (batch == 0 && notifyTime == 0 && restTime == 0 && updateTime == 0 && n == 0) {
		flag.PrintDefaults()
		fmt.Println("example run command: worker -batch=8000 -notify_time=5 -rest_time=5 -update_time=20 -n=100")

		return
	}

	if batch == 0 || notifyTime == 0 || restTime == 0 || updateTime == 0 || n == 0 {
		fmt.Printf("all parameters are required")

		return
	}

	clients := make([]*mongo.Client, consts.WorkerCount)
	for i := 0; i < consts.WorkerCount; i++ {
		client, err := mongodb.Client()
		if err != nil {
			logger.Errorf("try %d MongoDB client of %d, %+v", i, consts.WorkerCount, errors.Trace(err))
		}
		clients[i] = client
		defer client.Disconnect(context.TODO())
	}

	start := time.Now()
	logger.Infof("start %d workers with %d iterations", consts.WorkerCount, n)

	fetchRangeSet := stats.NewRangeSet(consts.WorkerCount)
	updateRangeSet := stats.NewRangeSet(consts.WorkerCount)

	wg := new(sync.WaitGroup)
	wg.Add(consts.WorkerCount)
	for i := 0; i < consts.WorkerCount; i++ {
		go func(index int) {
			matchStats, updateStats := worker(
				mongodb.Collection(clients[index]),
				index,
				time.Duration(notifyTime)*time.Second,
				time.Duration(restTime)*time.Second,
				int32(updateTime),
				int(batch),
				int(n),
			)

			fetchRangeSet.Add(matchStats)
			updateRangeSet.Add(updateStats)

			wg.Done()
		}(i)
	}

	wg.Wait()

	duration := time.Since(start)
	logger.Infof("worker complete by %d nanoseconds", duration)

	stats.IntervalStatsFetch(fetchRangeSet)
	stats.IntervalStatsUpdate(updateRangeSet)
}

func worker(collection *mongo.Collection, workerID int, notifyTime, restTime time.Duration, updateTime int32, batch, n int) (*stats.WorkerRange, *stats.WorkerRange) {
	matchStats := stats.NewWorkerRange(n, stats.MatchFast, stats.MatchNormal)
	updateStats := stats.NewWorkerRange(n, stats.UpdateFast, stats.UpdateNormal)

	for i := 0; i < n; i++ {
		matchStart := time.Now()
		subscribers, err := mongodb.Match(collection, workerID, timezones(i), now(), batch)
		matchDuration := time.Now().Sub(matchStart)
		subscriberCount := len(subscribers)

		// fetch error
		if err != nil {
			logger.Errorf(
				"worker: %2d; iteration: %5d; cnt: %5d; fetch: %6.3f; err %+v",
				workerID, i, subscriberCount, toS(matchDuration), err,
			)

			// sleep before next iteration
			time.Sleep(restTime)

			continue
		}

		if subscriberCount == 0 {
			// sleep before next iteration
			time.Sleep(restTime)

			continue
		}

		matchStats.Add(matchDuration, subscriberCount)

		// emulate notify
		time.Sleep(notifyTime)

		ids := make([]int32, subscriberCount)
		for j := 0; j < subscriberCount; j++ {
			ids[j] = subscribers[j].ID
		}

		updateStart := time.Now()
		err = mongodb.Update(collection, ids, now()+updateTime)
		updateDuration := time.Now().Sub(updateStart)

		// update error
		if err != nil {
			logger.Errorf(
				"worker: %2d; iteration: %5d; cnt: %5d; fetch: %6.3f; update: %6.3f; err %+v",
				workerID, i, subscriberCount, toS(matchDuration), toS(updateDuration), err,
			)

			// sleep before next iteration
			time.Sleep(restTime)

			continue
		}

		// update success
		logger.Infof(
			"worker: %2d; iteration: %5d; cnt: %5d; fetch: %6.3f; update: %6.3f",
			workerID, i, subscriberCount, toS(matchDuration), toS(updateDuration),
		)

		updateStats.Add(updateDuration, subscriberCount)

		// sleep before next iteration
		time.Sleep(restTime)
	}

	return matchStats, updateStats
}

func timezones(i int) []int {
	result := make([]int, 8)

	for j := 0; j < 8; j++ {
		current := i + j

		result[j] = current % consts.TimezoneCount
	}

	return result
}

func now() int32 {
	return int32(time.Now().Truncate(time.Second).Unix())
}

func toS(duration time.Duration) float64 {
	return math.Ceil(float64(duration/1e6)) / 1e3
}
