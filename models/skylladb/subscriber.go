package scylladb

type Subscriber struct {
	ID          uint32
	NotifyAfter uint32
	//Partner     uint32
	//Stream      uint32
	//Widget      uint32
	WorkerID uint8
	Timezone uint8
	//OS          uint8
	//Browser     uint8
	//Country     string
	//Language    string
	Endpoint string
}
