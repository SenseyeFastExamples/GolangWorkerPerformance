package aerospike

type Credentials struct {
	Hostname string
	Port     int
}
