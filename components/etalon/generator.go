package etalon

import (
	"math/rand"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/dataprovider"
	"workerperformance/models/etalon"
)

func Generate(shift, workerCount, timezoneCount, limit int) []etalon.Subscriber {
	result := make([]etalon.Subscriber, 0, limit)

	return GenerateAppend(result, shift, workerCount, timezoneCount, limit)
}

func GenerateAppend(dst []etalon.Subscriber, shift, workerCount, timezoneCount, limit int) []etalon.Subscriber {
	now := time.Now()

	rand.Seed(now.Unix())

	startNotifyAfter := uint32(0)

	for i := 0; i < limit; i++ {
		dst = append(dst, etalon.Subscriber{
			ID:          uint32(shift + i + 1),
			NotifyAfter: startNotifyAfter,
			Partner:     uint32(i),
			Stream:      uint32(i),
			Widget:      uint32(i),
			WorkerID:    uint8(rand.Intn(workerCount)),
			Timezone:    uint8(rand.Intn(timezoneCount)),
			OS:          uint8(i % 8),
			Browser:     uint8(i % 8),
			Country:     dataprovider.NextCountry(),
			Language:    dataprovider.NextLanguage(),
			Endpoint:    consts.Endpoint,
		})

		startNotifyAfter += 1
	}

	return dst
}
