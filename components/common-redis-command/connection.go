package command

import "github.com/go-redis/redis"

type connection func() (*redis.Client, error)
type connectionByNode func(node int) (*redis.Client, error)
