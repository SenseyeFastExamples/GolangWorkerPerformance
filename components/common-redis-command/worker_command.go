package command

import (
	"flag"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/juju/errors"
	"sync"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
	service "workerperformance/components/redis"
	"workerperformance/components/roundtime"
	"workerperformance/components/stats"
)

func WorkerCommand(connection connection, match service.MatchHandle) {
	var (
		helpFlag       = flag.Bool("help", false, "help")
		batchFlag      = flag.Uint("batch", 0, "subscribers fetch limit")
		notifyTimeFlag = flag.Uint("notify_time", 0, "seconds sleep to emulate send")
		restTimeFlag   = flag.Uint("rest_time", 0, "seconds sleep after send")
		updateTimeFlag = flag.Uint("update_time", 0, "seconds when subscribers pack will available to next send")
		nFlag          = flag.Uint("n", 0, "iterations")
	)

	flag.Parse()

	var (
		help       = *helpFlag
		batch      = *batchFlag
		notifyTime = *notifyTimeFlag
		restTime   = *restTimeFlag
		updateTime = *updateTimeFlag
		n          = *nFlag
	)

	if help || (batch == 0 && notifyTime == 0 && restTime == 0 && updateTime == 0 && n == 0) {
		flag.PrintDefaults()
		fmt.Println("example run command: worker -batch=8000 -notify_time=5 -rest_time=5 -update_time=20 -n=100")

		return
	}

	if batch == 0 || notifyTime == 0 || restTime == 0 || updateTime == 0 || n == 0 {
		fmt.Printf("all parameters are required")

		return
	}

	clients := make([]*redis.Client, consts.WorkerCount)
	for i := 0; i < consts.WorkerCount; i++ {
		client, err := connection()
		if err != nil {
			logger.Errorf("try %d Redis client of %d, %+v", i, consts.WorkerCount, errors.Trace(err))

			return
		}
		clients[i] = client
		defer client.Close()
	}

	start := time.Now()
	logger.Infof("start %d workers with %d iterations", consts.WorkerCount, n)

	fetchRangeSet := stats.NewRangeSet(consts.WorkerCount)
	updateRangeSet := stats.NewRangeSet(consts.WorkerCount)

	wg := new(sync.WaitGroup)
	wg.Add(consts.WorkerCount)
	for i := 0; i < consts.WorkerCount; i++ {
		go func(index int) {
			matchStats, updateStats := worker(
				clients[index],
				match,
				index,
				time.Duration(notifyTime)*time.Second,
				time.Duration(restTime)*time.Second,
				uint32(updateTime),
				int(batch),
				int(n),
			)

			fetchRangeSet.Add(matchStats)
			updateRangeSet.Add(updateStats)

			wg.Done()
		}(i)
	}

	wg.Wait()

	duration := time.Since(start)
	logger.Infof("worker complete by %d nanoseconds", duration)

	stats.IntervalStatsFetch(fetchRangeSet)
	stats.IntervalStatsUpdate(updateRangeSet)
}

func worker(client *redis.Client, match service.MatchHandle, workerID int, notifyTime, restTime time.Duration, updateTime uint32, batch, n int) (*stats.WorkerRange, *stats.WorkerRange) {
	matchStats := stats.NewWorkerRange(n, stats.MatchFast, stats.MatchNormal)
	updateStats := stats.NewWorkerRange(n, stats.UpdateFast, stats.UpdateNormal)

	for i := 0; i < n; i++ {
		matchStart := time.Now()
		subscribers, err := match(client, uint8(workerID), uint8(i%consts.TimezoneCount), roundtime.Now(), batch)
		matchDuration := time.Now().Sub(matchStart)
		subscriberCount := len(subscribers)

		// fetch error
		if err != nil {
			logger.Errorf(
				"worker: %2d; iteration: %5d; cnt: %5d; fetch: %6.3f; err %+v",
				workerID, i, subscriberCount, stats.ToS(matchDuration), err,
			)

			// sleep before next iteration
			time.Sleep(restTime)

			continue
		}

		if subscriberCount == 0 {
			//logger.Infof(
			//	"worker: %2d; iteration: %5d; cnt: %5d; empty fetch: %6.3f;",
			//	workerID, i, subscriberCount, toS(matchDuration),
			//)

			// sleep before next iteration
			time.Sleep(restTime)

			continue
		}

		matchStats.Add(matchDuration, subscriberCount)

		// emulate notify
		time.Sleep(notifyTime)

		notifyAfter := roundtime.Now() + updateTime

		updateStart := time.Now()
		_, err = service.Update(client, subscribers, notifyAfter)
		updateDuration := time.Now().Sub(updateStart)

		// update error
		if err != nil {
			logger.Errorf(
				"worker: %2d; iteration: %5d; cnt: %5d; fetch: %6.3f; update: %6.3f; err %+v",
				workerID, i, subscriberCount, stats.ToS(matchDuration), stats.ToS(updateDuration), err,
			)

			// sleep before next iteration
			time.Sleep(restTime)

			continue
		}

		// update success
		logger.Infof(
			"worker: %2d; iteration: %5d; cnt: %5d; fetch: %6.3f; update: %6.3f",
			workerID, i, subscriberCount, stats.ToS(matchDuration), stats.ToS(updateDuration),
		)

		updateStats.Add(updateDuration, subscriberCount)

		// sleep before next iteration
		time.Sleep(restTime)
	}

	return matchStats, updateStats
}
