package mongodb

import (
	"math/rand"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/dataprovider"
	"workerperformance/models/mongodb"
)

func Generate(shift int32, workerCount, timezoneCount, limit int) []mongodb.Subscriber {
	now := time.Now()

	rand.Seed(now.Unix())

	startNotifyAfter := int32(0)

	result := make([]mongodb.Subscriber, limit)
	for i := 0; i < limit; i++ {
		result[i] = mongodb.Subscriber{
			ID:          shift + int32(i+1),
			NotifyAfter: startNotifyAfter,
			Partner:     int32(i),
			Stream:      int32(i),
			Widget:      int32(i),
			WorkerID:    uint8(rand.Intn(workerCount)),
			Timezone:    uint8(rand.Intn(timezoneCount)),
			OS:          uint8(i % 8),
			Browser:     uint8(i % 8),
			Country:     dataprovider.NextCountry(),
			Language:    dataprovider.NextCountry(),
			Endpoint:    consts.Endpoint,
		}

		startNotifyAfter += 1
	}

	return result
}
