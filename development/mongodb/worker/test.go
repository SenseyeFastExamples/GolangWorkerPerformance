package main

import (
	"github.com/juju/errors"
	"go.mongodb.org/mongo-driver/mongo"
	"io/ioutil"
	"sync"
	"time"
	"workerperformance/components/logger"
	"workerperformance/components/mongodb"
	"workerperformance/components/oplogger"
)

const (
	SubscriberCount int = 2e7
	WorkerCount         = 20
	TimezoneCount       = 24
	Batch               = 8000
	NotifyTime          = 5 * time.Second
	RestTime            = 5 * time.Second
	UpdateShift         = 20 // seconds
	N                   = 50
)

var (
	operationLogger = oplogger.NewLogger(WorkerCount * 2 * N)
)

func main() {
	client, err := mongodb.Client()
	if err != nil {
		logger.Critical(errors.Trace(err))
	}

	collection := mongodb.Collection(client)

	if false {
		start := time.Now()
		subscribers := mongodb.Generate(0, WorkerCount, TimezoneCount, SubscriberCount)
		duration := time.Since(start)
		logger.Infof("generate %d subscribers by %d nanoseconds", SubscriberCount, duration)

		start = time.Now()
		err = mongodb.FillBatch(collection, subscribers)
		duration = time.Since(start)
		logger.Infof("store %d subscribers by %d nanoseconds", SubscriberCount, duration)

		if err != nil {
			logger.Critical(errors.Trace(err))
		}
	}

	if false {
		start := time.Now()
		err = mongodb.Index(collection)
		duration := time.Since(start)
		logger.Infof("index %d subscribers by %d nanoseconds", SubscriberCount, duration)

		if err != nil {
			logger.Critical(errors.Trace(err))
		}
	}

	clients := make([]*mongo.Client, WorkerCount)
	for i := 0; i < WorkerCount; i++ {
		client, err := mongodb.Client()
		if err != nil {
			logger.Critical(errors.Trace(err))
		}
		clients[i] = client
	}

	start := time.Now()
	logger.Infof("start %d workers with %d iterations", WorkerCount, N)

	wg := new(sync.WaitGroup)
	wg.Add(WorkerCount)
	for i := 0; i < WorkerCount; i++ {
		go func(index int) {
			worker(mongodb.Collection(clients[index]), index)

			wg.Done()
		}(i)
	}

	wg.Wait()

	duration := time.Since(start)
	logger.Infof("worker complete by %d nanoseconds", duration)

	err = ioutil.WriteFile("/go/src/workerperformance/result/worker-mongodb.txt", operationLogger.Dump(), 0666)
	if err != nil {
		logger.Critical(errors.Trace(err))
	}

	err = operationLogger.DumpPlot(
		WorkerCount,
		N,
		"/go/src/workerperformance/result/worker-mongodb-match.png",
		"/go/src/workerperformance/result/worker-mongodb-update.png",
		"/go/src/workerperformance/result/worker-mongodb-complex.png",
	)
	if err != nil {
		logger.Critical(errors.Trace(err))
	}
}

func worker(collection *mongo.Collection, workerID int) {
	for i := 0; i < N; i++ {
		logger.Infof("worker %d start %d iteration", workerID, i)

		start := time.Now()
		subscribers, err := mongodb.Match(collection, workerID, timezones(i), now(), Batch)
		duration := time.Now().Sub(start)

		logger.Error(errors.Trace(err))

		success := err == nil

		length := len(subscribers)

		operationLogger.LogMatch(
			int32(start.Unix()),
			workerID,
			i,
			length,
			int64(duration),
			success,
		)

		time.Sleep(NotifyTime)

		if success {
			ids := make([]int32, length)
			for j := 0; j < length; j++ {
				ids[j] = subscribers[j].ID
			}

			start := time.Now()
			err = mongodb.Update(collection, ids, now()+UpdateShift)
			duration := time.Now().Sub(start)

			success := err == nil
			operationLogger.LogUpdate(
				int32(start.Unix()),
				workerID,
				i,
				length,
				int64(duration),
				success,
			)
		}

		time.Sleep(RestTime)
	}
}

func timezones(i int) []int {
	result := make([]int, 8)

	for j := 0; j < 8; j++ {
		current := i + j

		result[j] = current % TimezoneCount
	}

	return result
}

func now() int32 {
	return int32(time.Now().Truncate(time.Second).Unix())
}
