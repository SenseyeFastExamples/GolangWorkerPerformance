package aerospike

import (
	"math/rand"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/dataprovider"
	"workerperformance/models/aerospike"
)

func Generate(shift, workerCount, timezoneCount int, startNotifyAfter uint32, limit int) []aerospike.Subscriber {
	now := time.Now()

	rand.Seed(now.Unix())

	result := make([]aerospike.Subscriber, limit)
	for i := 0; i < limit; i++ {
		result[i] = aerospike.Subscriber{
			ID:          uint32(shift + i + 1),
			NotifyAfter: startNotifyAfter,
			Partner:     uint32(i),
			Stream:      uint32(i),
			Widget:      uint32(i),
			WorkerID:    uint8(rand.Intn(workerCount)),
			Timezone:    uint8(rand.Intn(timezoneCount)),
			OS:          uint8(i % 8),
			Browser:     uint8(i % 8),
			Country:     dataprovider.NextCountry(),
			Language:    dataprovider.NextLanguage(),
			Endpoint:    consts.Endpoint,
		}

		startNotifyAfter += 1
	}

	return result
}
