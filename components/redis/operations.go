package redis

import (
	"github.com/go-redis/redis"
	"github.com/golang/protobuf/proto"
	"github.com/gopereza/packer"
	"github.com/juju/errors"
	"strconv"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
	protos "workerperformance/models/protos/redis"
	models "workerperformance/models/redis"
)

const (
	shards   = 16
	hashPack = 256  // < 400 proto size bytes
	keyPack  = 128  // < 400 proto size bytes
	zSetPack = 4096 // "xxxxxxxxxx" "xxxxxxxxxx" = 26 bytes

	zSetCount = consts.WorkerCount * consts.TimezoneCount
)

func FlushDB(client *redis.Client) error {
	return client.FlushDB().Err()
}

func StoreShardHashes(client *redis.Client, subscribers []models.Subscriber) (int, error) {
	sortedSetGroup := make(map[uint16][]models.Subscriber, zSetCount)
	shardedHashGroup := make(map[uint16][]models.Subscriber, shards)

	for _, subscriber := range subscribers {
		// sorted set
		key := uint16(subscriber.WorkerID)<<8 | uint16(subscriber.Timezone)

		values, ok := sortedSetGroup[key]
		if ok {
			sortedSetGroup[key] = append(values, subscriber)
		} else {
			values := make([]models.Subscriber, 0, 16) // 16 preallocate

			sortedSetGroup[key] = append(values, subscriber)
		}

		// hash
		shard := uint16(subscriber.ID % shards)
		shardedHashGroup[shard] = append(shardedHashGroup[shard], subscriber)
	}

	err := storeSortedSet(client, sortedSetGroup)
	if err != nil {
		return 0, err
	}

	for shard, values := range shardedHashGroup {
		key := subscriberHash(shard)
		length := len(values)

		packs := packer.Pack(length, hashPack)
		for _, pack := range packs {
			hash, err := marshalList(values[pack.From:pack.To])
			if err != nil {
				return 0, errors.Trace(err)
			}

			err = client.HMSet(key, hash).Err()
			if err != nil {
				return 0, errors.Trace(err)
			}
		}
	}

	return len(sortedSetGroup), nil
}

func StoreKeys(client *redis.Client, subscribers []models.Subscriber) (int, error) {
	sortedSetGroup := make(map[uint16][]models.Subscriber, zSetCount)

	for _, subscriber := range subscribers {
		// sorted set
		key := uint16(subscriber.WorkerID)<<8 | uint16(subscriber.Timezone)

		values, ok := sortedSetGroup[key]
		if ok {
			sortedSetGroup[key] = append(values, subscriber)
		} else {
			values := make([]models.Subscriber, 0, 16) // 16 preallocate

			sortedSetGroup[key] = append(values, subscriber)
		}
	}

	err := storeSortedSet(client, sortedSetGroup)
	if err != nil {
		return 0, err
	}

	packs := packer.Pack(len(subscribers), keyPack)
	buffer := make([]interface{}, 0, 2*keyPack)
	for _, pack := range packs {
		packSubscribers := subscribers[pack.From:pack.To]

		pairs := buffer[:0]
		for _, subscriber := range packSubscribers {
			hash, err := marshal(subscriber)
			if err != nil {
				return 0, errors.Trace(err)
			}

			pairs = append(pairs, subscriberKey(subscriber.ID), hash)
		}

		err := client.MSet(pairs...).Err()
		if err != nil {
			return 0, errors.Trace(err)
		}
	}

	return len(sortedSetGroup), nil
}

func Update(client *redis.Client, subscribers []models.Subscriber, notifyAfter uint32) (int, error) {
	sortedSetGroup := make(map[uint16][]redis.Z, 0)

	max := 0
	next := 0

	score := float64(notifyAfter)

	for _, subscriber := range subscribers {
		key := uint16(subscriber.WorkerID)<<8 | uint16(subscriber.Timezone)

		values, ok := sortedSetGroup[key]

		member := redis.Z{
			Score:  score,
			Member: subscriber.ID,
		}

		if ok {
			next = len(values) + 1

			sortedSetGroup[key] = append(values, member)
		} else {
			next = 1

			values := make([]redis.Z, 0, 16) // 16 preallocate

			sortedSetGroup[key] = append(values, member)
		}

		if next > max {
			next = max
		}
	}

	for key, values := range sortedSetGroup {
		workerID := uint8(key >> 8)
		timezone := uint8(key)

		cmd := client.ZAdd(
			workerTimezoneSortedSet(workerID, timezone),
			values...,
		)

		err := cmd.Err()
		if err != nil {
			return 0, err
		}
	}

	return len(sortedSetGroup), nil
}

func MatchShardHashes(client *redis.Client, workerID uint8, timezone uint8, now uint32, limit int) ([]models.Subscriber, error) {
	cmd := client.ZRangeByScoreWithScores(workerTimezoneSortedSet(workerID, timezone), redis.ZRangeBy{
		Max:   strconv.FormatUint(uint64(now), 10),
		Count: int64(limit),
	})

	members, err := cmd.Result()
	if err != nil {
		return nil, err
	}

	length := len(members)
	if length == 0 {
		return nil, nil
	}

	preResult := make([]models.Subscriber, 0, length)
	shardedHashGroup := make(map[uint16][]string, shards)

	for _, member := range members {
		idString := member.Member.(string)
		id, err := strconv.ParseUint(idString, 10, 64)

		if err != nil {
			logger.Error(errors.Trace(err))

			continue
		}

		shard := uint16(id % shards)
		shardedHashGroup[shard] = append(shardedHashGroup[shard], idString)

		preResult = append(preResult, models.Subscriber{
			ID:          uint32(id),
			WorkerID:    workerID,
			Timezone:    timezone,
			NotifyAfter: uint32(member.Score),
		})
	}

	idSubscriberMap := make(map[uint32]*protos.Subscriber, length)
	for shard, ids := range shardedHashGroup {
		hash := subscriberHash(shard)

		packs := packer.Pack(len(ids), hashPack)

		for _, pack := range packs {
			values, err := client.HMGet(
				hash,
				ids[pack.From:pack.To]...,
			).Result()

			if err != nil {
				logger.Error(errors.Trace(err))

				continue
			}

			for i, value := range values {
				data, ok := value.(string) // try before []byte -> ok = false

				if ok {
					message := &protos.Subscriber{}

					err := proto.Unmarshal([]byte(data), message)
					if err != nil {
						logger.Error(errors.Trace(err))

						continue
					}

					// already parsed before, proposal store
					id, err := strconv.ParseUint(ids[i], 10, 64)
					if err != nil {
						logger.Error(errors.Trace(err))

						continue
					}

					idSubscriberMap[uint32(id)] = message
				}
			}
		}
	}

	// to reuse memory
	result := preResult[:0]

	return merge(result, preResult, idSubscriberMap), nil
}

func MatchKeys(client *redis.Client, workerID uint8, timezone uint8, now uint32, limit int) ([]models.Subscriber, error) {
	cmd := client.ZRangeByScoreWithScores(workerTimezoneSortedSet(workerID, timezone), redis.ZRangeBy{
		Max:   strconv.FormatUint(uint64(now), 10),
		Count: int64(limit),
	})

	members, err := cmd.Result()
	if err != nil {
		return nil, err
	}

	length := len(members)
	if length == 0 {
		return nil, nil
	}

	preResult := make([]models.Subscriber, 0, length)
	shardedHashGroup := make(map[uint16][]string, shards)

	for _, member := range members {
		idString := member.Member.(string)
		id, err := strconv.ParseUint(idString, 10, 64)

		if err != nil {
			logger.Error(errors.Trace(err))

			continue
		}

		preResult = append(preResult, models.Subscriber{
			ID:          uint32(id),
			WorkerID:    workerID,
			Timezone:    timezone,
			NotifyAfter: uint32(member.Score),
		})
	}

	idSubscriberMap := make(map[uint32]*protos.Subscriber, length)
	packs := packer.Pack(len(preResult), keyPack)

	bufferKeys := make([]string, 0, keyPack)
	bufferIDs := make([]uint32, 0, keyPack)

	for _, pack := range packs {
		keys := bufferKeys[:0]
		ids := bufferIDs[:0]

		for i := pack.From; i < pack.To; i++ {
			id := preResult[i].ID
			ids = append(ids, id)
			keys = append(keys, subscriberKey(id))
		}

		subscriberProtoList, err := client.MGet(keys...).Result()
		if err != nil {
			return nil, errors.Trace(err)
		}

		for i, value := range subscriberProtoList {
			data, ok := value.(string) // try before []byte -> ok = false

			if ok {
				message := &protos.Subscriber{}

				err := proto.Unmarshal([]byte(data), message)
				if err != nil {
					logger.Error(errors.Trace(err))

					continue
				}

				id := ids[i]

				idSubscriberMap[id] = message
			}
		}
	}

	for shard, ids := range shardedHashGroup {
		hash := subscriberHash(shard)

		packs := packer.Pack(len(ids), hashPack)

		for _, pack := range packs {
			values, err := client.HMGet(
				hash,
				ids[pack.From:pack.To]...,
			).Result()

			if err != nil {
				logger.Error(errors.Trace(err))

				continue
			}

			for i, value := range values {
				data, ok := value.(string) // try before []byte -> ok = false

				if ok {
					message := &protos.Subscriber{}

					err := proto.Unmarshal([]byte(data), message)
					if err != nil {
						logger.Error(errors.Trace(err))

						continue
					}

					// already parsed before, proposal store
					id, err := strconv.ParseUint(ids[i], 10, 64)
					if err != nil {
						logger.Error(errors.Trace(err))

						continue
					}

					idSubscriberMap[uint32(id)] = message
				}
			}
		}
	}

	// to reuse memory
	result := preResult[:0]

	return merge(result, preResult, idSubscriberMap), nil
}

func merge(dst []models.Subscriber, list []models.Subscriber, idSubscriberMap map[uint32]*protos.Subscriber) []models.Subscriber {
	result := dst

	for _, subscriber := range list {
		message, ok := idSubscriberMap[subscriber.ID]

		if ok {
			result = append(result, models.Subscriber{
				ID:          subscriber.ID,
				NotifyAfter: subscriber.NotifyAfter,
				Partner:     message.Partner,
				Stream:      message.Stream,
				Widget:      message.Widget,
				OS:          uint8(message.Os),
				Browser:     uint8(message.Browser),
				WorkerID:    subscriber.WorkerID,
				Timezone:    subscriber.Timezone,
				Country:     message.Country,
				Language:    message.Language,
				Endpoint:    message.Endpoint,
			})
		}
	}

	return result
}

func workerTimezoneSortedSet(workerID, timezone uint8) string {
	const uint8size = 3
	const prefix = "z:subscribers:"
	const size = len(prefix) + 2*uint8size + 1

	buffer := make([]byte, 0, size)

	buffer = append(buffer, prefix...)
	buffer = strconv.AppendUint(buffer, uint64(workerID), 10)
	buffer = append(buffer, ':')
	buffer = strconv.AppendUint(buffer, uint64(timezone), 10)

	return string(buffer)
}

func subscriberHash(shard uint16) string {
	const uint16Size = 5
	const prefix = "h:shard:subscribers:"
	const size = len(prefix) + uint16Size

	buffer := make([]byte, 0, size)
	buffer = append(buffer, prefix...)
	buffer = strconv.AppendUint(buffer, uint64(shard), 10)

	return string(buffer)
}

func subscriberKey(id uint32) string {
	const uint32Size = 10
	const prefix = "k:subscribers:"
	const size = len(prefix) + uint32Size

	buffer := make([]byte, 0, size)
	buffer = append(buffer, prefix...)
	buffer = strconv.AppendUint(buffer, uint64(id), 10)

	return string(buffer)
}

func storeSortedSet(client *redis.Client, sortedSetGroup map[uint16][]models.Subscriber) error {
	buffer := make([]redis.Z, 0, zSetPack)

	for key, values := range sortedSetGroup {
		workerID := uint8(key >> 8)
		timezone := uint8(key)

		members := buffer[:0]
		for _, value := range values {
			members = append(members, redis.Z{
				Score:  float64(value.NotifyAfter),
				Member: value.ID,
			})
		}

		zSetKey := workerTimezoneSortedSet(workerID, timezone)

		packs := packer.Pack(len(values), zSetPack)
		for _, pack := range packs {
			packValues := values[pack.From:pack.To]
			members := buffer[:0]

			for _, value := range packValues {
				members = append(members, redis.Z{
					Score:  float64(value.NotifyAfter),
					Member: value.ID,
				})
			}

			cmd := client.ZAdd(
				zSetKey,
				members...,
			)

			err := cmd.Err()
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func marshalList(values []models.Subscriber) (map[string]interface{}, error) {
	hash := make(map[string]interface{}, len(values))

	for _, subscriber := range values {
		bytes, err := marshal(subscriber)
		if err != nil {
			return nil, errors.Trace(err)
		}
		id := strconv.FormatUint(uint64(subscriber.ID), 10)
		hash[id] = bytes
	}

	return hash, nil
}

func marshal(subscriber models.Subscriber) ([]byte, error) {
	message := &protos.Subscriber{
		Partner:  subscriber.Partner,
		Stream:   subscriber.Stream,
		Widget:   subscriber.Widget,
		Os:       uint32(subscriber.OS),
		Browser:  uint32(subscriber.Browser),
		Country:  subscriber.Country,
		Language: subscriber.Language,
		Endpoint: subscriber.Endpoint,
	}

	return proto.Marshal(message)
}
