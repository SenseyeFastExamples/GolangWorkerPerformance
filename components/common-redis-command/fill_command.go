package command

import (
	"flag"
	"fmt"
	"github.com/juju/errors"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/etalon"
	"workerperformance/components/logger"
	"workerperformance/components/redis"
)

func FillCommand(connection connectionByNode, flush redis.FlushHandle, store redis.StoreHandle) {
	helpFlag := flag.Bool("help", false, "help")
	nodeFlag := flag.Int("node", 0, "node")
	resetFlag := flag.Bool("reset", false, "Redis flush db")
	shiftFlag := flag.Int("shift", 0, "shift")
	limitFlag := flag.Int("n", 0, "generate & fill Redis sorted set")

	flag.Parse()

	help := *helpFlag
	reset := *resetFlag
	shift := *shiftFlag
	limit := *limitFlag
	node := *nodeFlag

	if help || (reset == false && limit == 0) {
		flag.PrintDefaults()
		fmt.Println("example run command: fill -node=0 -reset -n=1000000")
		fmt.Println("example run command: fill -reset -n=1000000 -shift=1000000")
		fmt.Println("example run command: fill -n=1000000")

		return
	}

	client, err := connection(node)
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
	defer client.Close()

	if reset {
		start := time.Now()
		err = flush(client)
		duration := time.Since(start)
		logger.Infof("Redis flush DB by %d nanoseconds", int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	if limit > 0 {
		start := time.Now()
		subscribers := etalon.Generate(shift, consts.WorkerCount, consts.TimezoneCount, limit)
		duration := time.Since(start)
		logger.Infof("Generate %d subscribers by %d nanoseconds", limit, int64(duration))

		start = time.Now()
		count, err := store(client, subscribers)
		duration = time.Since(start)
		logger.Infof("Redis add %d subscribers to %d sorted sets by %d nanoseconds", limit, count, int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}
}
