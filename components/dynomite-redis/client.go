package redis

import (
	"errors"
	"github.com/go-redis/redis"
	"workerperformance/components/env"
	"workerperformance/components/iterator"
	r "workerperformance/components/redis"
)

var (
	robin *iterator.StringInfinityCycle
)

var (
	ErrEmptyHost = errors.New("Dynomite redis empty hosts")
)

func Client() (*redis.Client, error) {
	if robin == nil {
		hosts := env.DynomiteRedisURI()

		if len(hosts) == 0 {
			return nil, ErrEmptyHost
		}

		robin = iterator.NewStringInfinityCycle(hosts)
	}

	return r.ClientByURI(robin.Next())
}

func ClientByNode(node int) (*redis.Client, error) {
	if robin == nil {
		hosts := env.DynomiteRedisURI()

		if len(hosts) == 0 {
			return nil, ErrEmptyHost
		}

		robin = iterator.NewStringInfinityCycle(hosts)
	}

	return r.ClientByURI(robin.ValueByIndex(node))
}
