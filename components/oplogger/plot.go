package oplogger

import (
	"fmt"
	"github.com/juju/errors"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
	"strconv"
)

func (l *Logger) DumpPlot(
	workerCount, n int,
	matchFilename string,
	updateFilename string,
	complexFilename string,
) error {
	err := storePNG("MongoDB read by workers", matchFilename, workerCount, n, l.workerMatchMap)
	if err != nil {
		return errors.Trace(err)
	}
	err = storePNG("MongoDB update by workers", updateFilename, workerCount, n, l.workerUpdateMap)
	if err != nil {
		return errors.Trace(err)
	}
	err = storeComplexPNG("MongoDB read & update by workers", complexFilename, workerCount, n, l.workerMatchMap, l.workerUpdateMap)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

func storePNG(title, filename string, workerCount, n int, workerLogMap map[int][]log) error {
	p, err := plot.New()
	if err != nil {
		return errors.Trace(err)
	}

	p.Title.Text = title
	p.X.Label.Text = "Iteration"
	p.Y.Label.Text = "Duration"

	for i := 0; i < workerCount; i++ {
		err = plotutil.AddLinePoints(p,
			strconv.Itoa(i), points(workerLogMap[i], n),
		)

		if err != nil {
			return errors.Trace(err)
		}
	}

	if err := p.Save(24*vg.Inch, 24*vg.Inch, filename); err != nil {
		return errors.Trace(err)
	}

	return nil
}

func storeComplexPNG(title, filename string, workerCount, n int, workerMatchMap, workerUpdateMap map[int][]log) error {
	p, err := plot.New()
	if err != nil {
		return errors.Trace(err)
	}

	p.Title.Text = title
	p.X.Label.Text = "Iteration"
	p.Y.Label.Text = "Duration"

	for i := 0; i < workerCount; i++ {
		err = plotutil.AddLinePoints(p,
			fmt.Sprintf("w %d r", i), points(workerMatchMap[i], n),
			fmt.Sprintf("w %d u", i), points(workerUpdateMap[i], n),
		)

		if err != nil {
			return errors.Trace(err)
		}
	}

	if err := p.Save(24*vg.Inch, 24*vg.Inch, filename); err != nil {
		return errors.Trace(err)
	}

	return nil
}

func points(logs []log, n int) plotter.XYs {
	pts := make(plotter.XYs, n)

	start := logs[0].time

	for i := 0; i < n; i++ {
		pts[i].X = float64(logs[i].time - start)
		pts[i].Y = float64(logs[i].duration / 1e6)
	}

	return pts
}
