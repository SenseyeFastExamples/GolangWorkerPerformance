package main

import (
	"workerperformance/components/common-redis-command"
	"workerperformance/components/redis"
)

func main() {
	command.WorkerCommand(redis.Client, redis.MatchShardHashes)
}
