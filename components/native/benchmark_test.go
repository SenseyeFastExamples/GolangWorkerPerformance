package native

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"workerperformance/components/mysql"
)

func BenchmarkRun(b *testing.B) {
	connection, err := mysql.Connection()
	if !assert.NoError(b, err) {
		return
	}
	defer connection.Close()

	var documents int
	for i := 0; i < b.N; i++ {
		documents = Run(connection)
	}

	b.Logf("documents %d", documents)
}
