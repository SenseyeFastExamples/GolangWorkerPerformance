package native

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"workerperformance/components/consts"
	"workerperformance/models/native"
)

func TestMatch(t *testing.T) {
	storage := NewStorage()

	const hour = 3600
	now := now()
	before1h := now - hour
	after1h := now + hour

	w1t1e1 := native.Subscriber{
		ID:          1,
		Timezone:    1,
		NotifyAfter: now,
		Endpoint:    "w1-t1-e1",
	}
	w1t1e2 := native.Subscriber{
		ID:          2,
		Timezone:    2,
		NotifyAfter: now,
		Endpoint:    "w1-t1-e2",
	}
	w1t1e3 := native.Subscriber{
		ID:          3,
		Timezone:    2,
		NotifyAfter: before1h,
		Endpoint:    "w1-t1-e3",
	}
	subscribers := []native.Subscriber{
		// matched
		w1t1e1,
		// matched
		w1t1e2,
		// matched
		w1t1e3,
		// skipped
		{
			ID:          4,
			Timezone:    3,
			NotifyAfter: after1h,
			Endpoint:    "w1-t1-e4",
		},
		// skipped
		{
			ID:          5,
			Timezone:    4,
			NotifyAfter: now,
			Endpoint:    "w1-t2-e5",
		},
		// skipped
		{
			ID:          6,
			Timezone:    5,
			NotifyAfter: now,
			Endpoint:    "w2-t1-e6",
		},
	}

	storage.Insert(subscribers)

	expect := []native.Subscriber{w1t1e1, w1t1e2, w1t1e3}

	actual, _ := storage.Match([]uint8{1, 2}, now, 10)

	assertSameSubscribers(t, expect, actual)
}

/**
BenchmarkStorage_Insert-4               	       5	1482119960 ns/op	1034974844 B/op	   57694 allocs/op
BenchmarkStorage_Insert_Preallocate-4   	       5	1255622179 ns/op	862083504 B/op	   57559 allocs/op
*/
func BenchmarkStorage_Insert(b *testing.B) {
	const limit = 3e6

	for i := 0; i < b.N; i++ {
		storage := NewStorage()

		now := now()

		storage.Insert(Generate(0, consts.TimezoneCount, now, limit))
	}
}

func BenchmarkStorage_Insert_Preallocate(b *testing.B) {
	const limit = 3e6

	now := now()
	subscribers := Generate(0, consts.TimezoneCount, now, limit)

	for i := 0; i < b.N; i++ {
		storage := NewStorage()

		storage.Insert(subscribers)
	}
}

/**
Before update:
BenchmarkMatch   	     500	   3833085 ns/op	  589836 B/op	       3 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:112: match count 1 of b.N = 1, match document count 8007 of 8000, total iteration count 682590
    main_test.go:112: match count 100 of b.N = 100, match document count 800700 of 800000, total iteration count 68263153
    main_test.go:112: match count 500 of b.N = 500, match document count 4003500 of 4000000, total iteration count 341309684
PASS
ok  	workerperformance/components/native	74.570s
After with fix:
BenchmarkMatch   	    2000	    797163 ns/op	  262153 B/op	       2 allocs/op
--- BENCH: BenchmarkMatch
    main_test.go:116: match count 1 of b.N = 1, match document count 8000 of 8000, total iteration count 32277
    main_test.go:116: match count 100 of b.N = 100, match document count 800000 of 800000, total iteration count 3195646
    main_test.go:116: match count 2000 of b.N = 2000, match document count 16000000 of 16000000, total iteration count 63962978
For 40e6 (80e6 OOM):
BenchmarkMatch-4   	   10000	   1106826 ns/op	  581640 B/op	       2 allocs/op
--- BENCH: BenchmarkMatch-4
    main_test.go:151: match count 1 of b.N = 1, match document count 8000 of 8000, total iteration count 32032
    main_test.go:151: match count 100 of b.N = 100, match document count 800000 of 800000, total iteration count 3198585
    main_test.go:151: match count 10000 of b.N = 10000, match document count 80000000 of 80000000, total iteration count 320446829

*/
func BenchmarkMatch(b *testing.B) {
	const (
		after  = 1e6
		before = 1e6
	)

	storage := NewStorage()

	now := now()

	storage.Insert(Generate(0, consts.TimezoneCount, now+1, after))
	storage.Insert(Generate(after, consts.TimezoneCount, now-before/2, before))

	matchCount := 0
	matchDocumentCount := 0
	totalIterationCount := 0

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		result, iterationCount := storage.Match(timezones(i), now, consts.Limit)

		documentCount := len(result)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}

		totalIterationCount += iterationCount
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d, total iteration count %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N, totalIterationCount)
}

/**
BenchmarkMatchWithUpdate   	    3000	  59830802 ns/op	  581640 B/op	       2 allocs/op
--- BENCH: BenchmarkMatchWithUpdate
    main_test.go:206: match count 1 of b.N = 1, match document count 8000 of 8000, total iteration count 31819
    main_test.go:206: match count 100 of b.N = 100, match document count 800000 of 800000, total iteration count 4373265
    main_test.go:206: match count 2893 of b.N = 3000, match document count 22660011 of 24000000, total iteration count 6364148769
PASS
ok  	workerperformance/components/native	266.098s
*/
func BenchmarkMatchWithUpdate(b *testing.B) {
	const (
		after  = 1e6
		before = 1e6
	)

	const (
		updateTime = 20
	)

	storage := NewStorage()

	current := now()

	storage.Insert(Generate(0, consts.TimezoneCount, current+1, after))
	storage.Insert(Generate(after, consts.TimezoneCount, current-before/2, before))

	matchCount := 0
	matchDocumentCount := 0
	totalIterationCount := 0

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		subscribers, iterationCount := storage.Match(timezones(i), now(), consts.Limit)

		documentCount := len(subscribers)
		if documentCount > 0 {
			matchDocumentCount += documentCount
			matchCount += 1
		}

		totalIterationCount += iterationCount

		storage.Update(subscribers, now()+updateTime)
	}

	b.Logf("match count %d of b.N = %d, match document count %d of %d, total iteration count %d", matchCount, b.N, matchDocumentCount, consts.Limit*b.N, totalIterationCount)
}

func assertSameSubscribers(t *testing.T, expect, actual []native.Subscriber) {
	t.Helper()

	if !assert.Equal(t, len(expect), len(actual)) {
		return
	}

	assert.Equal(t, idMap(expect), idMap(actual))
}

func idMap(subscriber []native.Subscriber) map[uint32]native.Subscriber {
	result := make(map[uint32]native.Subscriber)

	for _, subscriber := range subscriber {
		result[subscriber.ID] = subscriber
	}

	return result
}
