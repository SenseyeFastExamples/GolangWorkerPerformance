package native

import (
	"database/sql"
	"github.com/juju/errors"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
	"workerperformance/components/stats"
)

type Worker struct {
	workerID                uint8
	maxSubscriberID         uint32
	connection              *sql.DB
	storage                 *Storage
	subscriberIDUpdateQueue []uint32
	limit                   uint
}

func NewWorker(connection *sql.DB, workerID uint8, limit uint) *Worker {
	return &Worker{
		workerID:                workerID,
		maxSubscriberID:         0,
		connection:              connection,
		storage:                 NewStorage(),
		subscriberIDUpdateQueue: make([]uint32, 0, 8096),
		limit:                   limit,
	}
}

func (w *Worker) Run(i int, notifyTime time.Duration, updateTime uint32) error {
	start := time.Now()
	subscribers, err := Next(w.connection, w.workerID, w.maxSubscriberID, w.limit)
	dbRefreshDuration := time.Since(start)

	if err != nil {
		return errors.Trace(err)
	}

	dbLength := len(subscribers)
	if dbLength > 0 {
		w.storage.Insert(subscribers)

		maxSubscriber := subscribers[dbLength-1]
		w.maxSubscriberID = maxSubscriber.ID
	}

	start = time.Now()
	matched, iterationCount := w.storage.Match(timezones(i), now(), w.limit)
	nativeMatchDuration := time.Since(start)

	matchLength := len(matched)

	if matchLength == 0 {
		logger.Infof(
			"db refresh: %6.2f with %4d; native match: %6.2f; native update: %6.2f; db update: %6.2f; documents %4d; processed %7d; iteration %4d",
			stats.ToS(dbRefreshDuration), dbLength, stats.ToS(nativeMatchDuration), stats.ToS(0), stats.ToS(0), matchLength, iterationCount, i,
		)

		return errors.Trace(err)
	}

	// notify
	time.Sleep(notifyTime)

	// store to queue to update
	for _, subscriber := range matched {
		w.subscriberIDUpdateQueue = append(w.subscriberIDUpdateQueue, subscriber.ID)
	}

	nextNotifytAfter := now() + updateTime

	start = time.Now()
	w.storage.Update(matched, nextNotifytAfter)
	nativeUpdateDuration := time.Since(start)

	start = time.Now()
	err = update(w.connection, w.subscriberIDUpdateQueue, nextNotifytAfter)
	dbUpdateDuration := time.Since(start)

	logger.Infof(
		"db refresh: %6.2f with %4d; native match: %6.2f; native update: %6.2f; db update: %6.2f; documents %4d; processed %7d; iteration %4d",
		stats.ToS(dbRefreshDuration), dbLength, stats.ToS(nativeMatchDuration), stats.ToS(nativeUpdateDuration), stats.ToS(dbUpdateDuration), matchLength, iterationCount, i,
	)

	if err != nil {
		return errors.Trace(err)
	}

	w.subscriberIDUpdateQueue = w.subscriberIDUpdateQueue[:0]

	return nil
}

func timezones(i int) []uint8 {
	result := make([]uint8, 8)

	for j := 0; j < 8; j++ {
		current := i + j

		result[j] = uint8(current % consts.TimezoneCount)
	}

	return result
}

func now() uint32 {
	return uint32(time.Now().Truncate(time.Second).Unix())
}
