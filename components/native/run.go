package native

import (
	"database/sql"
	"github.com/juju/errors"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
)

func Run(connection *sql.DB) int {
	workerID := uint8(1)
	afterID := uint32(0)
	result := 0

	for {
		subscribers, err := Next(connection, workerID, afterID, consts.Limit)
		if err != nil {
			logger.Error(errors.Trace(err))

			return result
		}

		length := len(subscribers)
		if length == 0 {
			return result
		}

		afterID = subscribers[length-1].ID

		result += length
	}

	return result
}
