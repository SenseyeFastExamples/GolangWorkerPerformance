package bson

import (
	"github.com/stretchr/testify/assert"
	"gopkg.in/mgo.v2/bson"
	"testing"
	"workerperformance/models/mongodb"
)

var (
	dataProvider = []byte{
		175, 0, 0, 0, 16, 95, 105, 100, 0, 1, 0, 0, 0, 16, 110, 111, 116, 105, 102, 121, 95, 97, 102, 116, 101, 114, 0, 8, 190, 95, 93, 16, 112, 97, 114, 116, 110, 101, 114, 0, 113, 134, 169, 0, 16, 115, 116, 114, 101, 97, 109, 0, 114, 134, 169, 0, 16, 119, 105, 100, 103, 101, 116, 0, 115, 134, 169, 0, 16, 119, 111, 114, 107, 101, 114, 95, 105, 100, 0, 1, 0, 0, 0, 16, 116, 105, 109, 101, 122, 111, 110, 101, 0, 1, 0, 0, 0, 16, 111, 115, 0, 1, 0, 0, 0, 16, 98, 114, 111, 119, 115, 101, 114, 0, 1, 0, 0, 0, 2, 99, 111, 117, 110, 116, 114, 121, 0, 3, 0, 0, 0, 85, 65, 0, 2, 108, 97, 110, 103, 117, 97, 103, 101, 0, 3, 0, 0, 0, 117, 107, 0, 2, 101, 110, 100, 112, 111, 105, 110, 116, 0, 9, 0, 0, 0, 119, 49, 45, 116, 49, 45, 101, 49, 0, 0,
	}

	expect = &mongodb.Subscriber{
		ID:          1,
		NotifyAfter: 1566555656,
		Partner:     11110001,
		Stream:      11110002,
		Widget:      11110003,
		WorkerID:    1,
		Timezone:    1,
		OS:          1,
		Browser:     1,
		Country:     "UA",
		Language:    "uk",
		Endpoint:    "w1-t1-e1",
	}
)

func TestMgoUnmarshal(t *testing.T) {
	actual := new(mongodb.Subscriber)
	err := bson.Unmarshal(dataProvider, actual)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)
}

func BenchmarkMgoUnmarshal(b *testing.B) {
	actual := new(mongodb.Subscriber)

	for i := 0; i < b.N; i++ {
		_ = bson.Unmarshal(dataProvider, actual)
	}
}

func TestUnmarshal(t *testing.T) {
	actual := new(mongodb.Subscriber)
	err := Unmarshal(dataProvider, actual)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, expect, actual)
}

func BenchmarkUnmarshal(b *testing.B) {
	actual := new(mongodb.Subscriber)

	for i := 0; i < b.N; i++ {
		_ = Unmarshal(dataProvider, actual)
	}
}
