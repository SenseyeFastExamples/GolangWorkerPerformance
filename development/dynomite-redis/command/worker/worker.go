package main

import (
	"workerperformance/components/common-redis-command"
	"workerperformance/components/dynomite-redis"
	r "workerperformance/components/redis"
)

func main() {
	command.WorkerCommand(redis.Client, r.MatchKeys)
}
