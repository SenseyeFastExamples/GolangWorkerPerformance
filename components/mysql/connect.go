package mysql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"workerperformance/models/mysql"
)

func connect(params mysql.Credentials) (*sql.DB, error) {
	dsn := dns(params)

	return sql.Open("mysql", dsn)
}

func dns(p mysql.Credentials) string {
	return p.User + ":" + p.Pass + "@tcp(" + p.Host + ":" + p.Port + ")/" + p.Name
}
