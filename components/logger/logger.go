package logger

import (
	"log"
)

const (
	i = "[INFO]"
	e = "[ERROR]"
	c = "[CRITICAL]"
)

func Info(a string) {
	log.Print(i + " " + a)
}

func Infof(format string, a ...interface{}) {
	log.Printf(i+" "+format, a...)
}

func Error(err error) {
	if err == nil {
		return
	}

	Errorf("%+v", err)
}

func Errorf(format string, a ...interface{}) {
	log.Printf(e+" "+format, a...)
}

func Critical(err error) {
	Criticalf("%+v", err)
}

func Criticalf(format string, a ...interface{}) {
	log.Fatalf(c+" "+format, a...)
}
