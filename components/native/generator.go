package native

import (
	"math/rand"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/dataprovider"
	models "workerperformance/models/native"
)

func Generate(shift, timezoneCount int, startNotifyAfter uint32, limit int) []models.Subscriber {
	now := time.Now()

	rand.Seed(now.Unix())

	result := make([]models.Subscriber, limit)
	for i := 0; i < limit; i++ {
		result[i] = models.Subscriber{
			ID:          uint32(shift + i + 1),
			NotifyAfter: startNotifyAfter,
			Partner:     uint32(i),
			Stream:      uint32(i),
			Widget:      uint32(i),
			OS:          uint8(i % 8),
			Browser:     uint8(i % 8),
			Timezone:    uint8(rand.Intn(timezoneCount)),
			Country:     dataprovider.NextCountry(),
			Language:    dataprovider.NextLanguage(),
			Endpoint:    consts.Endpoint,
		}

		startNotifyAfter += 1
	}

	return result
}
