package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/juju/errors"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
	"workerperformance/components/mongodb"
)

func main() {
	helpFlag := flag.Bool("help", false, "help")
	resetFlag := flag.Bool("reset", false, "MongoDB collection drop & create")
	indexFlag := flag.Bool("index", false, "MongoDB collection add index")
	limitFlag := flag.Int("n", 0, "generate & fill MongoDB collection")

	flag.Parse()

	help := *helpFlag
	reset := *resetFlag
	index := *indexFlag
	limit := *limitFlag

	if help || (reset == false && limit == 0 && index == false) {
		flag.PrintDefaults()
		fmt.Println("example run command: fill -reset -n=1000000 -index")
		fmt.Println("example run command: fill -n=1000000")
		fmt.Println("example run command: fill -index")

		return
	}

	client, err := mongodb.Client()
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
	defer client.Disconnect(context.TODO())

	collection := mongodb.Collection(client)

	if reset {
		start := time.Now()
		err := mongodb.Drop(collection)
		duration := time.Since(start)
		logger.Infof("MongoDB collection drop by %d nanoseconds", int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	if limit > 0 {
		max, err := mongodb.Max(collection)
		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}

		start := time.Now()
		subscribers := mongodb.Generate(int32(max)+1, consts.WorkerCount, consts.TimezoneCount, limit)
		duration := time.Since(start)
		logger.Infof("Generate %d subscribers by %d nanoseconds", limit, int64(duration))

		start = time.Now()
		err = mongodb.FillBatch(collection, subscribers)
		duration = time.Since(start)
		logger.Infof("MongoDB collection add %d subscribers by %d nanoseconds", limit, int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	if index {
		start := time.Now()
		err := mongodb.Index(collection)
		duration := time.Since(start)
		logger.Infof("MongoDB collection add index by %d nanoseconds", int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}
}
