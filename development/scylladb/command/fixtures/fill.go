package main

import (
	"flag"
	"fmt"
	"github.com/gocql/gocql"
	"github.com/gopereza/packer"
	"github.com/juju/errors"
	"time"
	"workerperformance/components/consts"
	"workerperformance/components/logger"
	"workerperformance/components/scylladb"
	models "workerperformance/models/skylladb"
)

func main() {
	helpFlag := flag.Bool("help", false, "help")
	resetFlag := flag.Bool("reset", false, "ScyllaDB collection drop & create")
	indexFlag := flag.Bool("index", false, "ScyllaDB collection add index")
	singleFlag := flag.Bool("single-insert", false, "single insert")
	shiftFlag := flag.Int("shift", 0, "shift")
	limitFlag := flag.Int("n", 0, "generate & fill ScyllaDB collection")

	flag.Parse()

	help := *helpFlag
	reset := *resetFlag
	index := *indexFlag
	single := *singleFlag
	shift := *shiftFlag
	limit := *limitFlag

	if help || (reset == false && limit == 0 && index == false) {
		flag.PrintDefaults()
		fmt.Println("example run command: fill -reset -n=1000000 -index")
		fmt.Println("example run command: fill -reset -n=1000000 -shift=1000000 -index")
		fmt.Println("example run command: fill -reset -single-insert -n=1000000 -shift=1000000 -index")
		fmt.Println("example run command: fill -n=1000000")
		fmt.Println("example run command: fill -index")

		return
	}

	totalStart := time.Now()

	client, err := scylladb.ClientByTimeout(time.Minute)
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
	defer client.Close()

	if reset {
		start := time.Now()
		err = scylladb.Reset(client)
		duration := time.Since(start)
		logger.Infof("ScyllaDB drop & create by %d nanoseconds", int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	if limit > 0 {
		errCount := 0

		var fillHandle scylladb.FillHandle
		if single {
			fillHandle = scylladb.FillBatchByPrepare
		} else {
			fillHandle = scylladb.FillBatch
		}

		const batch int = 1e3
		subscribers := make([]models.Subscriber, 0, batch)

		packs := packer.Pack(limit, batch)
		for _, pack := range packs {
			err := fill(client, fillHandle, subscribers, shift+pack.From, batch)

			if err != nil {
				logger.Error(errors.Trace(err))

				errCount += 1

				continue
			}
		}

		if errCount > 0 {
			logger.Infof("ScyllaDB insert with %d errors", errCount)
		}
	}

	if index {
		start := time.Now()
		err := scylladb.Index(client)
		duration := time.Since(start)
		logger.Infof("ScyllaDB create materialized view (as index) by %d nanoseconds", int64(duration))

		if err != nil {
			logger.Error(errors.Trace(err))

			return
		}
	}

	totalDuration := time.Since(totalStart)
	logger.Infof("command complete by %d nanoseconds", int64(totalDuration))
}

func fill(client *gocql.Session, fillHandle scylladb.FillHandle, subscribers []models.Subscriber, shift, limit int) error {
	start := time.Now()
	subscribers = scylladb.GenerateAppend(subscribers[:0], shift, consts.WorkerCount, consts.TimezoneCount, limit)
	duration := time.Since(start)
	logger.Infof("Generate %d subscribers by %d nanoseconds", limit, int64(duration))

	start = time.Now()
	err := fillHandle(client, subscribers)
	duration = time.Since(start)
	logger.Infof("ScyllaDB insert %d subscribers by %d nanoseconds", limit, int64(duration))

	return err
}
