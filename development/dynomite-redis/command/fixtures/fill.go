package main

import (
	"workerperformance/components/common-redis-command"
	"workerperformance/components/dynomite-redis"
	r "workerperformance/components/redis"
)

func main() {
	command.FillCommand(redis.ClientByNode, redis.FlushDB, r.StoreKeys)
}
