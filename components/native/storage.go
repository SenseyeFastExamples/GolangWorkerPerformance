package native

import (
	"workerperformance/models/native"
)

type Storage struct {
	timezoneBucket map[uint8]map[uint32]native.Subscriber
}

func NewStorage() *Storage {
	return &Storage{
		timezoneBucket: make(map[uint8]map[uint32]native.Subscriber),
	}
}

func (s *Storage) Insert(subscribers []native.Subscriber) {
	for _, subscriber := range subscribers {
		key := subscriber.Timezone

		idMap, ok := s.timezoneBucket[key]
		if ok {
			idMap[subscriber.ID] = subscriber
		} else {
			// FIXME: maybe need add pre allocate sizes for all timezones
			idMap := make(map[uint32]native.Subscriber, 65536)

			idMap[subscriber.ID] = subscriber

			s.timezoneBucket[key] = idMap
		}
	}
}

func (s *Storage) Match(timezones []uint8, now uint32, limit uint) ([]native.Subscriber, int) {
	iterationCount := 0
	resultLength := uint(0)
	result := make([]native.Subscriber, 0, limit)

root:
	for _, timezone := range timezones {
		key := timezone

		idMap := s.timezoneBucket[key]

		for _, subscriber := range idMap {
			iterationCount += 1
			if subscriber.NotifyAfter <= now {
				result = append(result, subscriber)

				resultLength += 1
				if resultLength >= limit {
					break root
				}
			}
		}
	}

	return result, iterationCount
}

func (s *Storage) Update(subscribers []native.Subscriber, notifyAfter uint32) {
	for _, subscriber := range subscribers {
		key := subscriber.Timezone

		idMap := s.timezoneBucket[key]

		subscriber.NotifyAfter = notifyAfter

		idMap[subscriber.ID] = subscriber
	}
}
