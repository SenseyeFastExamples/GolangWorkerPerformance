package consts

const (
	WorkerCount   = 20
	TimezoneCount = 24
	Limit         = 8000
)

const Endpoint = "https://fcm.googleapis.com/fcm/send/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
const Batch = 1e5
