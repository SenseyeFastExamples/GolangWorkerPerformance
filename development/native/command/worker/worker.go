package main

import (
	"flag"
	"fmt"
	"github.com/juju/errors"
	"time"
	"workerperformance/components/logger"
	"workerperformance/components/mysql"
	"workerperformance/components/native"
)

func main() {
	var (
		helpFlag       = flag.Bool("help", false, "help")
		batchFlag      = flag.Uint("batch", 0, "subscribers fetch limit")
		notifyTimeFlag = flag.Uint("notify_time", 0, "seconds sleep to emulate send")
		restTimeFlag   = flag.Uint("rest_time", 0, "seconds sleep after send")
		updateTimeFlag = flag.Uint("update_time", 0, "seconds when subscribers pack will available to next send")
		nFlag          = flag.Int("n", 0, "iterations")
	)

	flag.Parse()

	var (
		help       = *helpFlag
		batch      = *batchFlag
		notifyTime = time.Duration(*notifyTimeFlag)
		restTime   = time.Duration(*restTimeFlag)
		updateTime = uint32(*updateTimeFlag)
		n          = *nFlag
	)

	if help || (batch == 0 && notifyTime == 0 && restTime == 0 && updateTime == 0 && n == 0) {
		flag.PrintDefaults()
		fmt.Println("example run command: worker -batch=8000 -notify_time=5 -rest_time=5 -update_time=20 -n=100")

		return
	}

	if batch == 0 || notifyTime == 0 || restTime == 0 || updateTime == 0 || n == 0 {
		fmt.Printf("all parameters are required")

		return
	}

	connection, err := mysql.Connection()
	if err != nil {
		logger.Error(errors.Trace(err))

		return
	}
	defer connection.Close()

	worker := native.NewWorker(connection, 1, batch)

	for i := 0; i < n; i++ {
		err := worker.Run(i, notifyTime, updateTime)

		logger.Error(errors.Trace(err))

		time.Sleep(restTime)
	}
}
